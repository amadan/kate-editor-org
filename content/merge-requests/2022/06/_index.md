---
title: Merge Requests - June 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/06/
---

#### Week 26

- **Kate - [Parse symbols when Symbol Viewer is shown](https://invent.kde.org/utilities/kate/-/merge_requests/779)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after 2 days.

- **Kate - [Search addon: don&#x27;t show the folder options for every new tab](https://invent.kde.org/utilities/kate/-/merge_requests/775)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 4 days.

- **Kate - [Replace QPixmap with icons from theme and update (all?) parsers to improve them](https://invent.kde.org/utilities/kate/-/merge_requests/769)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after 10 days.

- **Kate - [Improve fuzzy matching](https://invent.kde.org/utilities/kate/-/merge_requests/777)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Make modified doc&#x27;s tab&#x27;s text italic](https://invent.kde.org/utilities/kate/-/merge_requests/778)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [AppCommands: Do not quit application when last document or all documents are closed](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/380)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after 22 days.

- **Kate - [remove proxy style of avoid Windows issues](https://invent.kde.org/utilities/kate/-/merge_requests/774)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

#### Week 25

- **Kate - [KateFileTree addon: some improvements](https://invent.kde.org/utilities/kate/-/merge_requests/773)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **Kate - [KateApp: Respect modCloseAfterLast setting when closing document](https://invent.kde.org/utilities/kate/-/merge_requests/758)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after 20 days.

- **Kate - [Add configuration for colored brackets](https://invent.kde.org/utilities/kate/-/merge_requests/772)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Add a bit on non-obvious internals of snippet-scripts.](https://invent.kde.org/utilities/kate/-/merge_requests/764)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after 7 days.

- **KTextEditor - [KateThemeConfig: when copying a scheme, put the current name in the line-edit](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/385)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [implement output history limit](https://invent.kde.org/utilities/kate/-/merge_requests/771)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [emit stderr to our output view](https://invent.kde.org/utilities/kate/-/merge_requests/768)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Make snippet search case insensitive.](https://invent.kde.org/utilities/kate/-/merge_requests/770)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged at creation day.

#### Week 24

- **Kate - [Add filter for the Symbol Viewer](https://invent.kde.org/utilities/kate/-/merge_requests/762)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **Kate - [show no text for left/right sidebars](https://invent.kde.org/utilities/kate/-/merge_requests/763)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [use breeze class icon](https://invent.kde.org/utilities/kate/-/merge_requests/766)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Revert &quot;Do not move cursor when smart copying&quot;](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/384)**<br />
Request authored by [Fushan Wen](https://invent.kde.org/fusionfuture) and merged at creation day.

- **KSyntaxHighlighting - [Handle case when heredoc closing identifier is indented with space or tabulation](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/317)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **Kate - [disable projects restore in session per default](https://invent.kde.org/utilities/kate/-/merge_requests/761)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Update, fix and enhance PHP parser](https://invent.kde.org/utilities/kate/-/merge_requests/760)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **KTextEditor - [Utilize ECMDeprecationSettings to manage deprecate Qt API](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/383)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **KSyntaxHighlighting - [cmake.xml: Updates for CMake 3.24](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/318)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [Add OrderWithRequires and RemovePathPostfixes keywords to RPM Spec](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/316)**<br />
Request authored by [Marián Konček](https://invent.kde.org/mkoncek) and merged after one day.

- **KTextEditor - [allow to disable the autoreload if content is in git](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/382)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [Fix occurence highlighting not working with custom line height](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/381)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Adjust repo&#x27;s own includes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/313)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 17 days.

- **KTextEditor - [Do not move cursor when smart copying](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/377)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 17 days.

#### Week 23

- **Kate - [gdbplugin: Debugger Adapter Protocol backend](https://invent.kde.org/utilities/kate/-/merge_requests/741)**<br />
Request authored by [Héctor Mesa Jiménez](https://invent.kde.org/hectorm) and merged after 27 days.

- **Kate - [Add ability to show/hide parameters](https://invent.kde.org/utilities/kate/-/merge_requests/755)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after 13 days.

- **KSyntaxHighlighting - [avoid spell checking in diffs](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/315)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **Kate - [For KMoreTools includes for Qt6 builds](https://invent.kde.org/utilities/kate/-/merge_requests/759)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after one day.

- **Kate - [More simpler regexp to match different class declarations and slight refactoring in the Python parser](https://invent.kde.org/utilities/kate/-/merge_requests/753)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after 8 days.

- **Kate - [lspclient: code cleanups](https://invent.kde.org/utilities/kate/-/merge_requests/757)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [Allow hiding tab buttons in sidebars](https://invent.kde.org/utilities/kate/-/merge_requests/752)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 9 days.

- **Kate - [Remove colon left in some class declaration](https://invent.kde.org/utilities/kate/-/merge_requests/754)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after 8 days.

#### Week 22

- **KSyntaxHighlighting - [Avoid unprefixed CamelCase headers generated directly in build dir](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/314)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 2 days.

- **Kate - [Add 22.04.1 Windows release to appstream](https://invent.kde.org/utilities/kate/-/merge_requests/756)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

