---
title: KDE 4.5 is approaching, thanks to all Kate contributors
author: Christoph Cullmann

date: 2010-08-06T08:10:11+00:00
url: /2010/08/06/kde-4-5-is-approaching-thanks-to-all-kate-contributors/
categories:
  - Developers
  - KDE
  - Users
tags:
  - planet

---
KDE 4.5 will be released in the next days with the most polished Kate/KWrite and KatePart during the KDE 4.x series.

A lot of work went into fixing bugs and cleaning up old code for this release. Many important aspects where redone, just to enumerate a few:

  * encoding detection & handling
  * the text buffer
  * the undo/redo system (thanks Bernhard)
  * search/replace (thanks again Bernhard)
  * handling cursors and ranges
  * improved spell checking (thanks Michel)
  * improved indentation (thanks Milian)
  * speed improvements (Milian too)
  * better JS scripting (Dominik)
  * porting of KDevelop to new interfaces (David Nolden)

It will be the most unit-tested release of KatePart ever I guess, but still a long way to go until we have a good test coverage. (we just scratch the surface)

Many thanks to the people contributing to this release (not only to the ones named above or below!), without that much helping hands, never such an amount of cool stuff would have happened ;)

I guess one of the real kickoffs was the great Kate/KDevelop sprint in Berlin, handled by Milian and sponsored by the e.V., thanks a lot!

For the next release, with KDE 4.6, already new cool stuff is in production:

  * vim-like swapfiles by Diana (see [here][1] and [here][2])
  * the awesome [SQL plugin][3] by Marco

If you think, you can help us, just [join us][4] and make Kate for KDE 4.6 even more awesome.

 [1]: /2010/07/12/gsoc-swap-files-for-kate/
 [2]: /2010/07/27/gsoc-view-differences-for-kates-swap-files/
 [3]: /2010/07/29/katesql-a-new-plugin-for-kate/
 [4]: /join-us/