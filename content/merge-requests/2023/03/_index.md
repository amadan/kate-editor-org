---
title: Merge Requests - March 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/03/
---

#### Week 13

- **Kate - [Check if provider is persistent before deleting old items](https://invent.kde.org/utilities/kate/-/merge_requests/1175)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

- **KTextEditor - [Printing improvements](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/534)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Use internal diff view in KateMWonHDDialog](https://invent.kde.org/utilities/kate/-/merge_requests/1174)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Adjust to KPart taking it&#x27;s metadata in constructor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/527)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after 3 days.

- **KSyntaxHighlighting - [Ruby: highlight shorthand global interpolations](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/470)**<br />
Request authored by [Georg Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

- **Kate - [lspclient: also consider closest child of covering item in symbol outline view](https://invent.kde.org/utilities/kate/-/merge_requests/1171)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [Add QML language server configuration](https://invent.kde.org/utilities/kate/-/merge_requests/1172)**<br />
Request authored by [Magnus Groß](https://invent.kde.org/vimpostor) and merged after one day.

- **KTextEditor - [Draw caret ourselves](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/532)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix selection highlight for RTL text with custom line height](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/529)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Include range.js when initializing the engine](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/530)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Update doxyfile to 1.9.6](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/469)**<br />
Request authored by [Jeremy Murphy](https://invent.kde.org/jmurphy) and merged after 3 days.

- **Kate - [DiagnosticsView: Allow a provider to have persistent diagnostics](https://invent.kde.org/utilities/kate/-/merge_requests/1170)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [remove unused includes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/531)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Formatters don&#x27;t need Q_OBJECT macro](https://invent.kde.org/utilities/kate/-/merge_requests/1168)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Make KatePluginManager non QObject class](https://invent.kde.org/utilities/kate/-/merge_requests/1169)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use Qt Camelcase includes](https://invent.kde.org/utilities/kate/-/merge_requests/1167)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **KTextEditor - [KateUndo: Add null checks for TextLine](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/528)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Simplify KateUndo, replace inheritance hierarchy with single type](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/526)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 12

- **Kate - [Un-namespace KColorButton](https://invent.kde.org/utilities/kate/-/merge_requests/1166)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged at creation day.

- **KTextEditor - [KateUndoManager: Store KateUndoGroup by value](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/524)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fix update issues when removing last line](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/525)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add Flatpak CI/CD Integration](https://invent.kde.org/utilities/kate/-/merge_requests/1121)**<br />
Request authored by [Neelaksh Singh](https://invent.kde.org/neelusingh) and merged after 32 days.

- **Kate - [Allow opening multiple files from welcome view](https://invent.kde.org/utilities/kate/-/merge_requests/1162)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **KTextEditor - [Mark private methods of public classes as unexported](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/521)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **KTextEditor - [KateUndo simplify code](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/523)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [KateLayoutCache overhaul](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/522)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Optimize TextBuffer::blockForLine](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/516)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [DiffWidget: Use fileName to get syntax definition](https://invent.kde.org/utilities/kate/-/merge_requests/1165)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [OutputView: Dont steal focus](https://invent.kde.org/utilities/kate/-/merge_requests/1163)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [improvements for MWModOnHDDialog](https://invent.kde.org/utilities/kate/-/merge_requests/1164)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix RTL text with format incorrectly shaped](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/515)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Mark private methods of public classes as unexported](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/518)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [xml-indent: Optimize getCode](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/517)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Delete default constructors &amp; assign operators with public declaration](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/520)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [Use &quot;= delete&quot; to disable default constructors &amp; assign-operators](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/519)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [Mark pimpl constructors of public classes as unexported](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/468)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [Fix the translation template version number (5-&gt;6)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/514)**<br />
Request authored by [Luigi Toscano](https://invent.kde.org/ltoscano) and merged at creation day.

- **KSyntaxHighlighting - [earthfile.xml: update syntax according to the latest release 0.7](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/464)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 9 days.

#### Week 11

- **KTextEditor - [try to improve test stability](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/513)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Make tooltip about automatic disk realod clearer](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/502)**<br />
Request authored by [Tomáš Hnyk](https://invent.kde.org/felagund) and merged after 5 days.

- **KTextEditor - [Try to enable pch for autotests](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/511)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix implicit char casts in include classes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/510)**<br />
Request authored by [Ismael Asensio](https://invent.kde.org/iasensio) and merged at creation day.

- **Kate - [Fix missing explicit in single arg ctor warnings](https://invent.kde.org/utilities/kate/-/merge_requests/1154)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **Kate - [Formatting: Dont ignore original filePath/name](https://invent.kde.org/utilities/kate/-/merge_requests/1159)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fixes after enabling NO_CAST_FROM_ASCII for autotests](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/509)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Enable DQT_NO_CAST_FROM_ASCII for autotests](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/507)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Apply some clazy qt6 fixes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/506)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Remove QSharedPointer and QScopedPointer usage](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/508)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Remove Q_OBJECT macro from some classes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/504)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix assert](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/505)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Feature: Add ability to take screenshots of code](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/501)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **KTextEditor - [Make compilation faster](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/503)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [wildcard with path match in mode lines](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/500)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 7 days.

- **KSyntaxHighlighting - [TOML: add Cargo.lock to extension list](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/467)**<br />
Request authored by [Konrad Borowski](https://invent.kde.org/xfix) and merged after 3 days.

- **Kate - [Improve compilation times](https://invent.kde.org/utilities/kate/-/merge_requests/1158)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix clang-format unable to format improperly named files](https://invent.kde.org/utilities/kate/-/merge_requests/1157)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Show submodules in status](https://invent.kde.org/utilities/kate/-/merge_requests/1156)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [LSP: Don&#x27;t override insertText with newText if insertText is not empty](https://invent.kde.org/utilities/kate/-/merge_requests/1150)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Add PCH support for faster compilations](https://invent.kde.org/utilities/kate/-/merge_requests/1149)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KSyntaxHighlighting - [Fix the translation template number (5-&gt;6)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/466)**<br />
Request authored by [Luigi Toscano](https://invent.kde.org/ltoscano) and merged at creation day.

#### Week 10

- **Kate - [[gdbplugin] suppress output for expected errors (gdbmi)](https://invent.kde.org/utilities/kate/-/merge_requests/1152)**<br />
Request authored by [Héctor Mesa Jiménez](https://invent.kde.org/hectorm) and merged at creation day.

- **Kate - [improve detaching](https://invent.kde.org/utilities/kate/-/merge_requests/1153)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [fork into the background if we don&#x27;t need to be blocking](https://invent.kde.org/utilities/kate/-/merge_requests/1151)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [remove own terminal widget in favor of Konsole](https://invent.kde.org/utilities/kate/-/merge_requests/1147)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Avoid updating config all the time when saving](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/499)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Allow to build against kuserfeedback qt6/qt5](https://invent.kde.org/utilities/kate/-/merge_requests/1146)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [Use startHostProcess](https://invent.kde.org/utilities/kate/-/merge_requests/1145)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Misc fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1144)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Update CSharp LSP setting to use project path](https://invent.kde.org/utilities/kate/-/merge_requests/1142)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

- **Kate - [Formatting: Use lsp config style &quot;command&quot; array for command line](https://invent.kde.org/utilities/kate/-/merge_requests/1141)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Formatting plugin improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1139)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 09

- **Kate - [Fix KMoreTools in KF6](https://invent.kde.org/utilities/kate/-/merge_requests/1140)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [Show an icon for colored brackets config page](https://invent.kde.org/utilities/kate/-/merge_requests/1138)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Introduce cursorToOffset](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/496)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Show call stack when there is an error in js script](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/498)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [indentation fixes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/489)**<br />
Request authored by [Ilia Kats](https://invent.kde.org/iliakats) and merged after 8 days.

- **KTextEditor - [Fix cstyle tests](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/495)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KSyntaxHighlighting - [Bash: fix ${!#}](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/463)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Makefile: fix variable declaration, comment and semicolon in value and...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/462)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Show error messages from debugger if configurations are not correctly set](https://invent.kde.org/utilities/kate/-/merge_requests/1137)**<br />
Request authored by [Tarcisio Fischer](https://invent.kde.org/tarcisiofischer) and merged at creation day.

- **KSyntaxHighlighting - [Comment highlighting for doxyfile](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/460)**<br />
Request authored by [Lukas Sommer](https://invent.kde.org/sommer) and merged at creation day.

- **KSyntaxHighlighting - [Replace QVector by QList](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/461)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **Kate - [buildplugin: Fix copy&amp;select-all shortcut handling](https://invent.kde.org/utilities/kate/-/merge_requests/1136)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [Improve cstyle performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/493)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Improve performance of rendering spaces with dyn wrap disabled](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/494)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Highlight the QML &quot;required&quot; keyword, added in Qt 5.15](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/459)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **Kate - [[gdbplugin] fixes for multiple and moving breakpoints](https://invent.kde.org/utilities/kate/-/merge_requests/1135)**<br />
Request authored by [Héctor Mesa Jiménez](https://invent.kde.org/hectorm) and merged after one day.

- **KTextEditor - [Implement some more QAccessibleTextInterface functions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/492)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [documentSaveCopyAs: Use async job api](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/490)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Optimize rendering spaces with dyn wrapping](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/491)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Git: set SSH_ASKPASS env var](https://invent.kde.org/utilities/kate/-/merge_requests/1134)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

