---
title: Merge Requests - October 2019
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2019/10/
---

#### Week 42

- **Kate - [Multi project ctags](https://invent.kde.org/utilities/kate/-/merge_requests/43)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [Update documentation of highlight &amp; RegExp](https://invent.kde.org/utilities/kate/-/merge_requests/40)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Bump required version of qt to 5.10](https://invent.kde.org/utilities/kate/-/merge_requests/41)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged at creation day.

#### Week 40

- **Kate - [Project ctags](https://invent.kde.org/utilities/kate/-/merge_requests/36)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [External Tools: Translate name and category](https://invent.kde.org/utilities/kate/-/merge_requests/35)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 2 days.

- **Kate - [kate: make size of persistent toolview also persistent](https://invent.kde.org/utilities/kate/-/merge_requests/31)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 6 days.

- **kate-editor.org - [Add language selector and make the homepage translatable](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/7)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

- **Kate - [Projects Plugin: Support variables %{Project:Path} and %{Project:NativePath}](https://invent.kde.org/utilities/kate/-/merge_requests/34)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Add new custom value - KDE::windows_store](https://invent.kde.org/utilities/kate/-/merge_requests/33)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

