---
title: Merge Requests
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
menu:
  main:
    weight: 100
    parent: menu
---

This pages provides an overview of the merge requests we are current working on or did already accepted for the Kate, KTextEditor, KSyntaxHighlighting and kate-editor.org repositories.

## Currently Open Merge Requests
Our team is still working on the following requests. Feel free to take a look and help out, if any of them is interesting for you!
### Kate

- **[kateviewmanager: Welcome page handling improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1239)**<br />
Request authored by [Oliver Beard](https://invent.kde.org/olib).

- **[Draft: Added feature to synchronise scrolling between split views](https://invent.kde.org/utilities/kate/-/merge_requests/1203)**<br />
Request authored by [Ulterno Ω*](https://invent.kde.org/ulterno).

- **[Add Reopen closed Document option](https://invent.kde.org/utilities/kate/-/merge_requests/1078)**<br />
Request authored by [Igor Montagner](https://invent.kde.org/igormontagner).

### KTextEditor

- **[Draft: try to save changes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/546)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).


## Overall Accepted Merge Requests
- 1105 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 518 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 460 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 49 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



## Accepted Merge Requests of 2023

- 180 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 98 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 73 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 8 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [June 2023](/merge-requests/2023/06/) (16 requests)
* [May 2023](/merge-requests/2023/05/) (42 requests)
* [April 2023](/merge-requests/2023/04/) (50 requests)
* [March 2023](/merge-requests/2023/03/) (91 requests)
* [February 2023](/merge-requests/2023/02/) (72 requests)
* [January 2023](/merge-requests/2023/01/) (88 requests)


## Accepted Merge Requests of 2022

- 445 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 194 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 111 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 9 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2022](/merge-requests/2022/12/) (53 requests)
* [November 2022](/merge-requests/2022/11/) (55 requests)
* [October 2022](/merge-requests/2022/10/) (106 requests)
* [September 2022](/merge-requests/2022/09/) (53 requests)
* [August 2022](/merge-requests/2022/08/) (93 requests)
* [July 2022](/merge-requests/2022/07/) (43 requests)
* [June 2022](/merge-requests/2022/06/) (39 requests)
* [May 2022](/merge-requests/2022/05/) (37 requests)
* [April 2022](/merge-requests/2022/04/) (34 requests)
* [March 2022](/merge-requests/2022/03/) (91 requests)
* [February 2022](/merge-requests/2022/02/) (89 requests)
* [January 2022](/merge-requests/2022/01/) (66 requests)


## Accepted Merge Requests of 2021

- 348 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 177 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 144 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 24 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2021](/merge-requests/2021/12/) (51 requests)
* [November 2021](/merge-requests/2021/11/) (32 requests)
* [October 2021](/merge-requests/2021/10/) (45 requests)
* [September 2021](/merge-requests/2021/09/) (15 requests)
* [August 2021](/merge-requests/2021/08/) (34 requests)
* [July 2021](/merge-requests/2021/07/) (45 requests)
* [June 2021](/merge-requests/2021/06/) (57 requests)
* [May 2021](/merge-requests/2021/05/) (57 requests)
* [April 2021](/merge-requests/2021/04/) (37 requests)
* [March 2021](/merge-requests/2021/03/) (91 requests)
* [February 2021](/merge-requests/2021/02/) (109 requests)
* [January 2021](/merge-requests/2021/01/) (120 requests)


## Accepted Merge Requests of 2020

- 87 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 49 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 132 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 2 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2020](/merge-requests/2020/12/) (58 requests)
* [November 2020](/merge-requests/2020/11/) (28 requests)
* [October 2020](/merge-requests/2020/10/) (36 requests)
* [September 2020](/merge-requests/2020/09/) (42 requests)
* [August 2020](/merge-requests/2020/08/) (56 requests)
* [July 2020](/merge-requests/2020/07/) (10 requests)
* [June 2020](/merge-requests/2020/06/) (8 requests)
* [May 2020](/merge-requests/2020/05/) (14 requests)
* [April 2020](/merge-requests/2020/04/) (2 requests)
* [March 2020](/merge-requests/2020/03/) (4 requests)
* [February 2020](/merge-requests/2020/02/) (7 requests)
* [January 2020](/merge-requests/2020/01/) (5 requests)


## Accepted Merge Requests of 2019

- 45 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 6 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2019](/merge-requests/2019/12/) (7 requests)
* [November 2019](/merge-requests/2019/11/) (3 requests)
* [October 2019](/merge-requests/2019/10/) (9 requests)
* [September 2019](/merge-requests/2019/09/) (20 requests)
* [August 2019](/merge-requests/2019/08/) (12 requests)
