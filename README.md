# kate-editor.org

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_kate-editor-org)](https://binary-factory.kde.org/job/Website_kate-editor-org/)

This repository contains the full [kate-editor.org website](https://kate-editor.org).

On push the https://binary-factory.kde.org/job/Website_kate-editor-org/ job will trigger an update of the web server.

# Live preview / Development
Details about the shared theme: [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/)

If you don't want to touch the shared theme, just delete the _development_ config file `config/development/config.yaml` and uncomment the `hugo mod get ...` line in `server.sh`.

Then you can start a local hugo powered web server via

```bash
./server.sh
```

The command will print the URL to use for local previewing.

# I18n
[hugoi18n](https://invent.kde.org/websites/hugo-i18n)

# Update the syntax-highlighting framework update sites

Check out the README.md in the syntax-highlighting.git on invent.kde.org.

There is a build target to generate the needed stuff after a successful compile of the framework.

# Update auto-generated pages like "team" or "merge requests"

Just run

```bash
./regenerate.sh
```

and review the results before you commit them.
