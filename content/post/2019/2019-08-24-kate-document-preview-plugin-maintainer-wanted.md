---
title: Kate - Document Preview Plugin - Maintainer Wanted!
author: Christoph Cullmann
date: 2019-08-24T18:46:00+02:00
url: /post/2019/2019-08-24-kate-document-preview-plugin-maintainer-wanted/
---

At the moment the [*Document Preview* plugin](https://docs.kde.org/trunk5/en/applications/kate/kate-application-plugin-preview.html) that e.g. allows to preview Markdown
or other documents layout-ed via embedding a matching KPart is no longer maintained.

You can find more information about why the plugin got abandoned in this [phabricator ticket](https://phabricator.kde.org/D16668).

If you want to step up and keep that plugin alive and kicking, now is your chance!

Even if you don't want to maintain it, you can help out with taking care of existing
bugs for this plugin.

Just head over to the [KDE Bugzilla bugs](https://bugs.kde.org/buglist.cgi?bug_status=__open__&component=plugin-preview&list_id=1657308&product=kate) for this plugin.

Any help with this is welcome!
