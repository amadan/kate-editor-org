---
title: PHP treeview improvements in Kate
author: Emmanuel Bouthenot

date: 2010-10-27T19:04:31+00:00
url: /2010/10/27/php-treeview-improvements-in-kate/
categories:
  - Users
tags:
  - planet

---
<div>
  Being a regular Kate user (more than half a dozen hours each day), I was not satisfied by the PHP treeview (available from the SymbolViewer plugin). As some of you have probably noticed, it is quite outdated and seems to only support the PHP4 syntax.
</div>

<div>
  Here is a screenshot:
</div>

<div>
  <a href="/wp-content/uploads/2010/10/kate_phptree_old.png"><img class="size-medium wp-image-738 aligncenter" src="/wp-content/uploads/2010/10/kate_phptree_old-300x177.png" alt="" width="300" height="177" srcset="/wp-content/uploads/2010/10/kate_phptree_old-300x177.png 300w, /wp-content/uploads/2010/10/kate_phptree_old-1024x604.png 1024w, /wp-content/uploads/2010/10/kate_phptree_old.png 1280w" sizes="(max-width: 300px) 100vw, 300px" /></a>
</div>

<div>
  The recent switch of Kate to gitourious was the trigger to try compile it on my own and to dive into the source code. A few hours later, the PHP treeview was rewritten. It currently supports the PHP5 syntax (even the namespaces).
</div>

<div>
  Here is screenshot with the same file:
</div>

<div>
  <a href="/wp-content/uploads/2010/10/kate_phptree_new.png"><img class="aligncenter size-medium wp-image-739" src="/wp-content/uploads/2010/10/kate_phptree_new-300x177.png" alt="" width="300" height="177" srcset="/wp-content/uploads/2010/10/kate_phptree_new-300x177.png 300w, /wp-content/uploads/2010/10/kate_phptree_new-1024x604.png 1024w, /wp-content/uploads/2010/10/kate_phptree_new.png 1280w" sizes="(max-width: 300px) 100vw, 300px" /></a>
</div>

<div>
  I&#8217;ve submitted my patch to the Kate Team (via the mailing list) and it was kindly accepted and merged. Thanks.
</div>

<div>
  PS: Hi Planet KDE, this is my first post :)
</div>