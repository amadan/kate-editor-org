---
title: Evolution of Kate Development (Take 2)
author: Christoph Cullmann

date: 2011-11-06T14:31:15+00:00
url: /2011/11/06/evolution-of-kate-development-take-2/
pw_single_layout:
  - "1"
categories:
  - Common
  - KDE
  - Users
tags:
  - planet

---
Dominik&#8217;s video is cool, but we agreed that perhaps the names should be around and we can have a bit higher resolution to make them readable ;)  
Therefore here is a second take of the video, this time with names and 720p. (I have no luck with music, therefore, silence)

<center>
  <br /> <br />
</center>

You only get to see the video if you visit our blog page [here][1].  
Direct HD link to YouTube [here][2].

Command to create it (if YouTube eats it):

<pre>gource --multi-sampling --seconds-per-day 0.02 --auto-skip-seconds 0.1 --camera-mode track --stop-at-end -b 000000 --hide filenames,dirnames --disable-progress --date-format “%Y-%m” --viewport 1280x720 --output-ppm-stream - -r 60 | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libvpx -b 10000K -threads 4 kate-gource.webm
</pre>

 [1]: /2011/11/06/evolution-of-kate-development-take-2/
 [2]: http://youtu.be/qK4DVZIptt0?hd=1