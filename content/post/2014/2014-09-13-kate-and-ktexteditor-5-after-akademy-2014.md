---
title: Kate and KTextEditor 5 after Akademy 2014
author: Dominik Haumann

date: 2014-09-13T11:15:42+00:00
url: /2014/09/13/kate-and-ktexteditor-5-after-akademy-2014/
categories:
  - Common
tags:
  - planet

---
The yearly <a title="Akademy KDE Conference" href="https://akademy.kde.org/2014" target="_blank">KDE conference Akademy</a> just ended, so it&#8217;s time to look at what changed in the holy Kate in the Frameworks 5 land.

### KTextEditor Framework

  * <a title="Git reloading documents" href="http://lists.kde.org/?l=kde-commits&m=141051301229217&w=2" target="_blank">silent reload of document</a>: Switching a git branch, Kate always pops up a dialog asking whether to reload the document. With this patch, if the document is version controlled by git, the git hash of the file computed and then it&#8217;s checked whether the file exists in git. If so, the file is reloaded without asking you. This should be very handy for developers using git! Thanks to Sven Brauch for this idea!
  * new <a title="Kate Highlighting Unit Testing Infrastructure" href="http://lists.kde.org/?l=kde-commits&m=141029053616802&w=2" target="_blank">highlighting unit testing</a> infrastructure
  * several new syntax highlighting files
  * cursor down in the last line in the document moves the cursor to the end of the line, same for the cursor up behavior, after just 8 years a bugzilla wish becomes true ;)
  * properly load/save the search & replace history
  * fix kateversion tags in all our >200 highlighting files, thanks to Martin Walch
  * as always: lots of improvements to the vi input mode

### Kate Application

  * use native dialogs on all platforms, including OS X, Windows
  * split view: action to <a title="Kate5: Toggle splitter orientation" href="https://bugs.kde.org/show_bug.cgi?id=116826" target="_blank">toggle splitter orientation</a>
  * the toolbar is by default turned off (see screenshot), resulting in a very cleaned up interface. You can turn it on in the Settings menu.
  * new document switcher plugin (see screenshot) through Ctrl+Tab, providing quick access to the most recently used documents (similar to Alt+Tab in kwin), based on KDevelops code
  * improvements to the tab bar
  * revive, cleanup and improve the text snippet plugin by Sven Brauch
  * projects plugin: <a title="Autoloading of Kate Projects" href="http://lists.kde.org/?l=kde-commits&m=141029235617502&w=2" target="_blank">autoload project</a> even if no .kateproject is found (configurable to not clash with the auto-generated cmake .kateproject file), implemented by _Michal Humpula_
  * the Plasma 5 applet to start a Kate session is back, thanks to the work of Josef Wenninger

<img class="aligncenter size-full wp-image-3379" alt="Kate Document Switcher" src="/wp-content/uploads/2014/09/kate-documentswitcher.png" width="785" height="533" srcset="/wp-content/uploads/2014/09/kate-documentswitcher.png 785w, /wp-content/uploads/2014/09/kate-documentswitcher-300x203.png 300w" sizes="(max-width: 785px) 100vw, 785px" /> 

A big thanks to the organizers of this year&#8217;s Akademy, and a big thanks to all our <a title="Akademy 2014 Sponsors" href="https://akademy.kde.org/2014/sponsors" target="_blank">sponsors</a> and supporting members. The location was amazing and the venue allowed us all to have a very productive week! Looking forward to next year! :-)