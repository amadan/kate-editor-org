---
title: Fancy Terminal Prompt
author: Dominik Haumann

date: 2018-01-08T20:55:15+00:00
url: /2018/01/08/fancy-terminal-prompt/
categories:
  - Developers
  - KDE
  - Users
tags:
  - planet

---
By default, the terminal looks as follows on my Linux distribution:

<img class="aligncenter size-full wp-image-4108" src="/wp-content/uploads/2018/01/konsole.png" alt="" width="864" height="420" srcset="/wp-content/uploads/2018/01/konsole.png 864w, /wp-content/uploads/2018/01/konsole-300x146.png 300w, /wp-content/uploads/2018/01/konsole-768x373.png 768w" sizes="(max-width: 864px) 100vw, 864px" /> 

However, if you are working a lot on the terminal, there are a lot of scripts and tricks available in the net that improve the information displayed in the terminal in many ways. For instance, since many many years, I have the following at the end of my ~/.bashrc:

<pre># use a fancy prompt
PS1="\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]"
PS1="$PS1 \`if [ \$? = 0 ]; then echo -e '\[\033[01;32m\]:-)';"
PS1="$PS1 else echo -e '\[\033[01;31m\]:-(' \$?; fi\`\[\033[00m\]"
PS1="$PS1 \$(__git_ps1 \"(%s)\") \$ "</pre>

Once you open a new terminal, then the appearance is as follows:

<img class="aligncenter size-full wp-image-4109" src="/wp-content/uploads/2018/01/konsole-nice.png" alt="" width="864" height="420" srcset="/wp-content/uploads/2018/01/konsole-nice.png 864w, /wp-content/uploads/2018/01/konsole-nice-300x146.png 300w, /wp-content/uploads/2018/01/konsole-nice-768x373.png 768w" sizes="(max-width: 864px) 100vw, 864px" /> 

As you can see, now I have nice colors: The hostname is green, the folder is blue, the return value of the last executed command is a green :-) in case of success (exit code = 0), and a red :-( in case of errors (exit code != 0). In addition, the last part shows the current git branch (master). Showing the git branch is very useful especially if you are using arc a lot and work with many branches.

I am sure there are many more cool additions to the terminal. If you have some nice additions, please share &#8211; maybe also as a new blog?