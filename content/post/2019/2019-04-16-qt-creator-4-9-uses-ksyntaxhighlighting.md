---
title: Qt Creator 4.9 uses KSyntaxHighlighting
author: Christoph Cullmann

date: 2019-04-16T18:45:00+00:00
excerpt: |
  As you can read in the official Creator 4.9.0 release announcement, Qt Creator now uses the KSyntaxHighlighting Framework for providing the generic highlighting.
  This is a nice step for the wider adoption of this MIT licensed part of the KDE Frameworks...
url: /posts/qt-creator-4-9-uses-ksyntaxhighlighting/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/qt-creator-4-9-uses-ksyntaxhighlighting/
syndication_item_hash:
  - 22723404ac9548f4eb500f4b66b63882
categories:
  - Common

---
As you can read in the [official Creator 4.9.0 release announcement][1], Qt Creator now uses the [KSyntaxHighlighting Framework][2] for providing the [generic highlighting][3].

This is a nice step for the wider adoption of this [MIT][4] licensed part of the [KDE Frameworks][5].

And this is not just an one-way consumption of our work.

The framework got actively patches back that make it more usable for other consumers, too, like [Kate][6] ;=)

If you want concrete examples, take a look at:

  * D18982: [prevent assertion in regex load][7]
  * D18996: [Fix building with namespaced Qt][8]
  * D18997: [Add functions returning all definitions for a mimetype or file name][9]
  * D19200: [Return sorted definitions for file names and mime types][10]
  * D19215: [Install DefinitionDownloader header][11]

I hope this cooperation will continue in the future. I thank the people working on Qt Creator that made this integration possible. I hope the initial effort will pay of with less code for them to maintain on their own and more improvements of the framework for all users.

 [1]: https://blog.qt.io/blog/2019/04/15/qt-creator-4-9-0-released/
 [2]: https://api.kde.org/frameworks/syntax-highlighting/html/index.html
 [3]: https://doc.qt.io/qtcreator/creator-highlighting.html#generic-highlighting
 [4]: https://opensource.org/licenses/MIT
 [5]: https://api.kde.org/frameworks/index.html
 [6]: /
 [7]: https://phabricator.kde.org/D18982
 [8]: https://phabricator.kde.org/D18996
 [9]: https://phabricator.kde.org/D18997
 [10]: https://phabricator.kde.org/D19200
 [11]: https://phabricator.kde.org/D19215