---
title: Get Kate
hideMeta: true
author: Christoph Cullmann
date: 2010-07-09T14:40:05+00:00
sassFiles:
  - /scss/get-it.scss
menu:
  main:
    weight: 3
---

{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux

+  Install [Kate](https://apps.kde.org/en/kate) or [KWrite](https://apps.kde.org/en/kwrite) from [your distribution](https://kde.org/distributions/).

  {{< appstream_badges >}}

+  Install [Kate's Snap package from Snapcraft](https://snapcraft.io/kate).

  {{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+  Download [the release 64-bit AppImage for Kate](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/). *
+  Download [the nightly 64-bit AppImage for Kate](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/). **

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+  Install [Kate from the Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW).

  {{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}
  
+  {{< i18n "windows-release" "https://binary-factory.kde.org/job/Kate_Release_win64/lastSuccessfulBuild/artifact/" >}} *
+  Download [the nightly Kate 64-bit installer](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/). **
+  Install [Kate via Chocolatey](https://chocolatey.org/packages/kate). \*\*\*

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+  Download the [Kate release installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/). *
+  Download the [Kate nightly installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/). **

{{< /get-it >}}

{{< get-it src="/reusable-assets/git.svg" >}}

## Source Code

The [source code for Kate](https://invent.kde.org/utilities/kate) is available on KDE’s GitLab instance. For detailed instructions on how to build Kate from source, check the [Build it](/build-it) page.

{{< /get-it >}}


{{< get-it-info >}}

**About the releases:** <br>
[Kate](https://apps.kde.org/en/kate) and [KWrite](https://apps.kde.org/en/kwrite) are part of [KDE Applications](https://apps.kde.org), which are [released typically 3 times a year en-masse](https://community.kde.org/Schedules). The [text editing](https://api.kde.org/frameworks/ktexteditor/html/) and the [syntax highlighting](https://api.kde.org/frameworks/syntax-highlighting/html/) engines are provided by [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), which is [updated monthly](https://community.kde.org/Schedules/Frameworks). New releases are announced [here](https://kde.org/announcements/).

\* The **release** packages contain the latest version of Kate and KDE Frameworks.

\*\* The **nightly** packages are automatically compiled daily from source code, therefore they may be unstable and contain bugs or incomplete features. These are only recommended for testing purposes.

\*\*\* The Chocolatey packages are developed independently from KDE. It may or may not be up to date.

{{< /get-it-info >}}
