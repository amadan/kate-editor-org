---
title: Merge Requests - June 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/06/
---

#### Week 24

- **Kate - [Add clear button to input fields in config dialog and output view](https://invent.kde.org/utilities/kate/-/merge_requests/1238)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **Kate - [Fix qt6 build with pch](https://invent.kde.org/utilities/kate/-/merge_requests/1240)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Rename variables to fix two cppcheck warnings](https://invent.kde.org/utilities/kate/-/merge_requests/1241)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged at creation day.

- **Kate - [Fix various framework checks for KF6](https://invent.kde.org/utilities/kate/-/merge_requests/1242)**<br />
Request authored by [Aaron Dewes](https://invent.kde.org/aarondewes) and merged at creation day.

#### Week 23

- **KSyntaxHighlighting - [Highlight QML pragma keyword](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/487)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KSyntaxHighlighting - [Highlight QML pragma keyword](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/486)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **Kate - [Fix dependency on KF5NewStuff for project addon](https://invent.kde.org/utilities/kate/-/merge_requests/1237)**<br />
Request authored by [Aaron Dewes](https://invent.kde.org/aarondewes) and merged at creation day.

- **Kate - [ensure KTextEditor plugins separation between kf5/6](https://invent.kde.org/utilities/kate/-/merge_requests/1236)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 5 days.

- **Kate - [Fix loading konsolepart in Qt6](https://invent.kde.org/utilities/kate/-/merge_requests/1235)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 5 days.

- **KTextEditor - [emulatedcommandbar QList::first() on temporary fix](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/569)**<br />
Request authored by [Shreevathsa V M](https://invent.kde.org/shreevathsavm) and merged at creation day.

#### Week 22

- **Kate - [Do not change diagnostics filter after building with errors](https://invent.kde.org/utilities/kate/-/merge_requests/1233)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 4 days.

- **Kate - [diagnostics: add action and shortcut to clear diagnostics filtering](https://invent.kde.org/utilities/kate/-/merge_requests/1234)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 2 days.

- **KTextEditor - [Introduce: Kate::TextBlock::blockSize() and offsetToCursor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/565)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Remove blockForIndex](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/568)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Remove some useless alias functions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/567)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Reuse Kate::TextLine instance when inserting/removing](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/566)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

