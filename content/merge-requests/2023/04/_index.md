---
title: Merge Requests - April 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/04/
---

#### Week 17

- **KTextEditor - [Optimize text rendering for long lines](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/545)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged after 4 days.

- **Kate - [Rename GDB to Debug or GDB/DAP](https://invent.kde.org/utilities/kate/-/merge_requests/1207)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

- **Kate - [Use complementary colors for IO View](https://invent.kde.org/utilities/kate/-/merge_requests/1208)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

- **Kate - [rainbowparens: use document text modification signals](https://invent.kde.org/utilities/kate/-/merge_requests/1206)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [remove all dynamic_cast usage](https://invent.kde.org/utilities/kate/-/merge_requests/1205)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix hiding terminal in project view](https://invent.kde.org/utilities/kate/-/merge_requests/1204)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 3 days.

#### Week 16

- **KSyntaxHighlighting - [ConTeXt: various fixes, support some embeddings and add some missing title macros](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/479)**<br />
Request authored by [John Doe](https://invent.kde.org/vicsanrope) and merged at creation day.

- **KSyntaxHighlighting - [Backport theme changes to KF5. Now includes Catppuccin syntax highlighting themes.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/478)**<br />
Request authored by [M. Ibrahim](https://invent.kde.org/sourcastic) and merged at creation day.

- **KSyntaxHighlighting - [Add RPM .spec %generate_buildrequires section name](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/477)**<br />
Request authored by [Marián Konček](https://invent.kde.org/mkoncek) and merged after one day.

- **Kate - [PluginManager cleanup](https://invent.kde.org/utilities/kate/-/merge_requests/1202)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Tweak catppuccin themes&#x27; comment colours and add diff colours](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/476)**<br />
Request authored by [M. Ibrahim](https://invent.kde.org/sourcastic) and merged at creation day.

- **Kate - [Add colors and clickable links for info/warning/errors in build output](https://invent.kde.org/utilities/kate/-/merge_requests/1199)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 6 days.

- **KSyntaxHighlighting - [backport master hl file updates](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/475)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

#### Week 15

- **Kate - [ensure sidebars are properly collapsed](https://invent.kde.org/utilities/kate/-/merge_requests/1200)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Robot Framework syntax highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/474)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 8 days.

- **KTextEditor - [Fix indenting removes characters if line has tabs at start](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/540)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Enable &#x27;indent text on paste&#x27; by default](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/210)**<br />
Request authored by [Bharadwaj Raju](https://invent.kde.org/bharadwaj-raju) and merged after 532 days.

- **Kate - [Don&#x27;t ask user if he wants to save modified document when deleting](https://invent.kde.org/utilities/kate/-/merge_requests/1148)**<br />
Request authored by [Nikita Karpei](https://invent.kde.org/nkarpei) and merged after 30 days.

- **Kate - [Diagnostics: Simplify provider registeration and make it safer](https://invent.kde.org/utilities/kate/-/merge_requests/1198)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Git: Remove show num status config option](https://invent.kde.org/utilities/kate/-/merge_requests/1197)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use diagnostics for build plugin instead of adding marks separately](https://invent.kde.org/utilities/kate/-/merge_requests/1155)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 26 days.

- **Kate - [Git: Highlight active file in status view](https://invent.kde.org/utilities/kate/-/merge_requests/1196)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Create shortcuts for diagnostic view items](https://invent.kde.org/utilities/kate/-/merge_requests/1192)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 2 days.

- **Kate - [Git status: Adjust contrast of red/green if item is selected](https://invent.kde.org/utilities/kate/-/merge_requests/1195)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [change interpretation of trim_trailing_whitespace](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/538)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

#### Week 14

- **Kate - [GitStatus: Uniquify filenames in the status](https://invent.kde.org/utilities/kate/-/merge_requests/1194)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Relicense all my stuff under GPL2.0 or later](https://invent.kde.org/utilities/kate/-/merge_requests/1191)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [CompilerExplorer: Fix crash when minimizing](https://invent.kde.org/utilities/kate/-/merge_requests/1193)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Lower Qt deprecation level](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/539)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [Fix preview for markdown files not loaded on mode change](https://invent.kde.org/utilities/kate/-/merge_requests/1189)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Add mimetype for markdown](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/473)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Format: Use stdin for xmllint](https://invent.kde.org/utilities/kate/-/merge_requests/1188)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Diagnostic improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1186)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Include markdownpart in flatpak builds](https://invent.kde.org/utilities/kate/-/merge_requests/1190)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Cleanup kateproject.h includes](https://invent.kde.org/utilities/kate/-/merge_requests/1187)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix asking to save untitled empty doc on close](https://invent.kde.org/utilities/kate/-/merge_requests/1185)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Allow forcing RTL direction](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/537)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KSyntaxHighlighting - [Use all-upper-case version variable names (KF consistency), also K-prefix](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/472)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [Update flatpak manifest to be the same as release one](https://invent.kde.org/utilities/kate/-/merge_requests/1182)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Diagnostics: Allow filtering on provider](https://invent.kde.org/utilities/kate/-/merge_requests/1183)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Open link fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1184)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Intoduce open link plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1180)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Cherrypicks from master](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/536)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Konsole: Show error when konsole not found](https://invent.kde.org/utilities/kate/-/merge_requests/1181)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 13

- **Kate - [search: remove kitemviews dependency](https://invent.kde.org/utilities/kate/-/merge_requests/1179)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [use media-record for breakpoint as that icon is still missing](https://invent.kde.org/utilities/kate/-/merge_requests/1178)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Add icon colors to symbol list](https://invent.kde.org/utilities/kate/-/merge_requests/1173)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 4 days.

- **Kate - [Intoduce KateTextHintManager](https://invent.kde.org/utilities/kate/-/merge_requests/1176)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Add a remove trailing spaces action](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/535)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Correct escape sequence highlighting (no need for word boundary)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/471)**<br />
Request authored by [Lukas Sommer](https://invent.kde.org/sommer) and merged at creation day.

