---
title: New Website
author: Christoph Cullmann

date: 2019-04-06T16:00:00+00:00
excerpt: |
  After several years of failing to update my personal WordPress page, I restarted my website with the Hugo framework.
  The new website uses zero cookies (yes, no stupid cookie question) and no kind of analytic software.
  I hope my Impressum is done well e...
url: /posts/new-website/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/new-website/
syndication_item_hash:
  - 8bf813c5e4907a5d43172b4e1595167d
  - 8bf813c5e4907a5d43172b4e1595167d
  - c47d5edfbbb08274293c9ab290aae616
  - 97d34d40b422d4350c64cf8f8747370e
categories:
  - Common

---
After several years of failing to update my personal WordPress page, I restarted my website with the Hugo framework. The new website uses zero cookies (yes, no stupid cookie question) and no kind of analytic software. I hope my Impressum is done well enough to avoid any legal issues. At the moment the website is still very empty, I hope to fill in more content about my open source projects and academic stuff in the near future.
