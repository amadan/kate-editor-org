---
title: Kate LSP Status – July 22
author: Christoph Cullmann

date: 2019-07-22T20:16:00+00:00
excerpt: |
  After my series of LSP client posts, I got the question: What does this actually do? And why should I like this or help with it?
  For the basic question: What the heck is the Language Server Protocol (LSP), I think my first post can help. Or, for more d...
url: /posts/kate-lsp-status-july-22/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/kate-lsp-status-july-22/
syndication_item_hash:
  - 1a70c550d5cc92e5bdcdd8ba1809b06c
  - 008b6db606658699c1601ca7321257ec
  - aff8b5982f89c031f345c6947b69f7b4
categories:
  - Common

---
After my series of LSP client posts, I got the question: What does this actually do? And why should I like this or help with it?

For the basic question: What the heck is the [Language Server Protocol (LSP)][1], I think my [first post][2] can help. Or, for more details, just head over to the [official what/why/&hellip; page][3].

But easier than to describe why it is nice, I can just show the stuff in action. Below is a video that shows the features that at the moment work with our master branch. It is shown using the build directory of Kate itself.

To get a usable build directory, I build my stuff locally with [kdesrc-build][4], the only extra config I have in the global section of my **.kdesrc-buildrc** is:

> cmake-options -DCMAKE\_BUILD\_TYPE=RelWithDebInfo -G &ldquo;Kate - Unix Makefiles&rdquo; -DCMAKE\_EXPORT\_COMPILE_COMMANDS=ON

This will auto generate the needed **.kateproject** files for the Kate project plugin and the **compile_commands.json** for **clangd** (the LSP server for C/C++ the plugin uses).

If you manually build your stuff with **cmake**, you can just add the

> -G &ldquo;Kate - Unix Makefiles&rdquo; -DCMAKE\_EXPORT\_COMPILE_COMMANDS=ON

parts to your **cmake** call. If you use **ninja** and not **make**, just use

> -G &ldquo;Kate - Ninja&rdquo; -DCMAKE\_EXPORT\_COMPILE_COMMANDS=ON

Then, let&rsquo;s see what you can do, once you are in a prepared build directory and have a **master** version of **Kate** in your **PATH**.

<center>
  <a href="https://youtu.be/w0grp9npnNA" ><img width=500 src="https://cullmann.io/posts/kate-lsp-status-july-22/images/kate-lsp-video.jpg"></a>
</center>

I hope the quality is acceptable, that is my first try in a long time to do some screen-cast ;)

As you can see, this is already in an usable state at least for C/C++ in combination with **clangd**.

For details how to build Kate master with it&rsquo;s plugins, please take a look at [this guide][5].

If you want to start to hack on the plugin, you find it in the kate.git, [addons/lspclient][6].

Feel welcome to show up on <kwrite-devel@kde.org> and help out! All development discussions regarding this plugin happen there.

If you are already familiar with Phabricator, post some patch directly at [KDE&rsquo;s Phabricator instance][7].

 [1]: https://microsoft.github.io/language-server-protocol/overview
 [2]: https://cullmann.io/posts/kate-language-server-protocol-client/
 [3]: https://langserver.org/
 [4]: https://kdesrc-build.kde.org/
 [5]: /build-it/
 [6]: https://cgit.kde.org/kate.git/tree/addons/lspclient
 [7]: https://phabricator.kde.org/differential/