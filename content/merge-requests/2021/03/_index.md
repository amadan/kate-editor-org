---
title: Merge Requests - March 2021
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2021/03/
---

#### Week 13

- **kate-editor.org - [Add April 1 blog about git blame](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/28)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Change license from GPL 2.0+ to LGPL 2.0+](https://invent.kde.org/utilities/kate/-/merge_requests/359)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 12

- **Kate - [Highlighting/themes documentation: remove references to Kate&#x27;s flatpak package](https://invent.kde.org/utilities/kate/-/merge_requests/358)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after one day.

- **KTextEditor - [Don&#x27;t warn about unsaved changes when closing if blank and unsaved](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/131)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged after one day.

- **KTextEditor - [Use the text selection background color for the scrollbar minimap slider](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/129)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KSyntaxHighlighting - [java: add assert and var keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/187)**<br />
Request authored by [Denis Lisov](https://invent.kde.org/tanriol) and merged after one day.

- **KSyntaxHighlighting - [cmake.xml: Updates for CMake 3.20](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/188)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **Kate - [Use more icons in git plugin](https://invent.kde.org/utilities/kate/-/merge_requests/357)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Work/clazy tool](https://invent.kde.org/utilities/kate/-/merge_requests/353)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 6 days.

- **KSyntaxHighlighting - [PHP: add preg_last_error_msg() function and classes that was previously a resource](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/186)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Js: numeric separator ; fix Octal ; add new classes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/185)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Remove unused ref to KateView* from KateAnnotationItemDelegate](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/127)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [avoid gaps in indentation line drawing](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/126)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [ensure output view scrolls correctly](https://invent.kde.org/utilities/kate/-/merge_requests/356)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Use some of the new git icons in the git integration](https://invent.kde.org/utilities/kate/-/merge_requests/349)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 5 days.

- **KTextEditor - [don&#x27;t use F9 &amp; F10 as shortcuts](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/125)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Remove unused includes + use more fwd decls](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/124)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Make it possible to compile with Qt 6](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/184)**<br />
Request authored by [David Schulz](https://invent.kde.org/davidschulz) and merged at creation day.

- **KSyntaxHighlighting - [Php 8.0 and 8.1](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/183)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Fix file/folder input dialog label](https://invent.kde.org/utilities/kate/-/merge_requests/350)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [Add basic touchscreen support](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/123)**<br />
Request authored by [Daniel Tang](https://invent.kde.org/danielt) and merged after 4 days.

- **KSyntaxHighlighting - [Fix #5 and add Path style with alternate value (${xx:-/path/})](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/182)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Markdown: add folding in sections](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/179)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after one day.

- **KSyntaxHighlighting - [Python: add match and case keywords (Python 3.10)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/181)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Disable spellchecking in Rust code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/180)**<br />
Request authored by [William Wold](https://invent.kde.org/wmww) and merged after one day.

#### Week 11

- **Kate - [Use hybrid, deterministic + cursorPositionChanged for location history](https://invent.kde.org/utilities/kate/-/merge_requests/352)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Tell user what to do instead of using sudo or kdesu](https://invent.kde.org/utilities/kate/-/merge_requests/354)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged after one day.

- **Kate - [Use colors from KColorScheme](https://invent.kde.org/utilities/kate/-/merge_requests/348)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Fix remote branch comparing](https://invent.kde.org/utilities/kate/-/merge_requests/347)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add a widget to view projects TODOs/FIXMEs in project info toolview](https://invent.kde.org/utilities/kate/-/merge_requests/343)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Fix jump-forward action not enabled.](https://invent.kde.org/utilities/kate/-/merge_requests/351)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [[Vimode] Fix/Improve Paragraph and Sentence text objects](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/121)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KTextEditor - [Port away from deprecated KCompletion signals](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/122)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **Kate - [git compare branches](https://invent.kde.org/utilities/kate/-/merge_requests/340)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Fix i18n in commit dialog and use breeze-danger-red color](https://invent.kde.org/utilities/kate/-/merge_requests/346)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Correct minor typo](https://invent.kde.org/utilities/kate/-/merge_requests/345)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Restrict horizontal range of cursor to avoid unintentionally wrapping.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/120)**<br />
Request authored by [Pat Brown](https://invent.kde.org/patbrown) and merged at creation day.

- **KSyntaxHighlighting - [Fix BracketMatching color in Breeze Dark theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/178)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Put () in front when autocompleting functions](https://invent.kde.org/utilities/kate/-/merge_requests/342)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Make sure hover close button is visible on non-kde platforms](https://invent.kde.org/utilities/kate/-/merge_requests/341)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 10

- **Kate - [Make filetree toolbar hidable](https://invent.kde.org/utilities/kate/-/merge_requests/339)**<br />
Request authored by [M  B ](https://invent.kde.org/brenneis) and merged at creation day.

- **Kate - [Viewspace settings on startup improvements](https://invent.kde.org/utilities/kate/-/merge_requests/295)**<br />
Request authored by [Marcell Fulop](https://invent.kde.org/marekful) and merged after 15 days.

- **Kate - [use markdown directly in lsptooltip](https://invent.kde.org/utilities/kate/-/merge_requests/338)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add default external tools C++, KDE and Qt API](https://invent.kde.org/utilities/kate/-/merge_requests/337)**<br />
Request authored by [Thiago Sueto](https://invent.kde.org/thiagosueto) and merged after one day.

- **Kate - [Show a close button on hover in Documents plugin](https://invent.kde.org/utilities/kate/-/merge_requests/282)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 18 days.

- **Kate - [Allow opening any directory from kate as project](https://invent.kde.org/utilities/kate/-/merge_requests/336)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Allow treeview to retain focus for keyboard navigation](https://invent.kde.org/utilities/kate/-/merge_requests/333)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Allow creating new file/folder in project](https://invent.kde.org/utilities/kate/-/merge_requests/332)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Parse groovy files with the parseCppSymbols() - same as Java](https://invent.kde.org/utilities/kate/-/merge_requests/331)**<br />
Request authored by [roman 149](https://invent.kde.org/romanonefournine) and merged after 2 days.

- **Kate - [Fix text not centered in commit dialog subject](https://invent.kde.org/utilities/kate/-/merge_requests/334)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Also update the indent/tab width limit to 200 in VariableLineEdit](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/118)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **Kate - [Use a single fixed view for viewing diffs](https://invent.kde.org/utilities/kate/-/merge_requests/335)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [turn on line numbers &amp; modification markers](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/117)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Add support for fluent language files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/176)**<br />
Request authored by [Fabian Wunsch](https://invent.kde.org/fabi) and merged after 4 days.

- **Kate - [Allow opening results in new doc + use full doc text if no selection](https://invent.kde.org/utilities/kate/-/merge_requests/329)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Avoid static QScrollBar instance](https://invent.kde.org/utilities/kate/-/merge_requests/330)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **KSyntaxHighlighting - [Add `findloc` Fortran function](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/177)**<br />
Request authored by [Jakub Benda](https://invent.kde.org/albandil) and merged at creation day.

- **Kate - [Update syntax highlighting documentation](https://invent.kde.org/utilities/kate/-/merge_requests/327)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Re-add history actions to mainwindow](https://invent.kde.org/utilities/kate/-/merge_requests/328)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Add option to keep spaces to the left of cursor when saving](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/112)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 6 days.

- **Kate - [Fix histories for viewspaces are mixed](https://invent.kde.org/utilities/kate/-/merge_requests/326)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Manually get file&#x27;s relative path to dir in while creating project](https://invent.kde.org/utilities/kate/-/merge_requests/325)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Disable search commands while searching/replacing](https://invent.kde.org/utilities/kate/-/merge_requests/322)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **Kate - [Refactor projectplugin git ls-files](https://invent.kde.org/utilities/kate/-/merge_requests/324)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Allow cancelling git operations](https://invent.kde.org/utilities/kate/-/merge_requests/321)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

#### Week 09

- **Kate - [S&amp;R: Add UseUnicodePropertiesOption to regexps](https://invent.kde.org/utilities/kate/-/merge_requests/323)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [Use unicode regex ;=)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/113)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Fix unit test](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/115)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [Search: Enable Unicode support in QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/114)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [[Vimode] Improve search wrapped hint](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/107)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 12 days.

- **Kate - [Create Git process on demand](https://invent.kde.org/utilities/kate/-/merge_requests/320)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Configurable single / double click in git-status view](https://invent.kde.org/utilities/kate/-/merge_requests/316)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Dont depend on gitwidget process in StashDialog](https://invent.kde.org/utilities/kate/-/merge_requests/319)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add shortcut for showing git commit info](https://invent.kde.org/utilities/kate/-/merge_requests/317)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Allow to stash changes on Close](https://invent.kde.org/utilities/kate/-/merge_requests/228)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged after 26 days.

- **Kate - [Show modified lines count in git status](https://invent.kde.org/utilities/kate/-/merge_requests/311)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use build icon for Build Selected Target](https://invent.kde.org/utilities/kate/-/merge_requests/315)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Move forward/backward actions to mainToolBar](https://invent.kde.org/utilities/kate/-/merge_requests/313)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Enable the back button when navigating forward.](https://invent.kde.org/utilities/kate/-/merge_requests/312)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Hide/show history jump buttons with the tab-bar](https://invent.kde.org/utilities/kate/-/merge_requests/310)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Add jumping back and forth between locations](https://invent.kde.org/utilities/kate/-/merge_requests/306)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Git push: initial commit](https://invent.kde.org/utilities/kate/-/merge_requests/308)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **kate-editor.org - [Remove impressum](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/27)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged at creation day.

- **Kate - [Implement Git show file history](https://invent.kde.org/utilities/kate/-/merge_requests/294)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [Use a more explicit description for the modified files dialog option](https://invent.kde.org/utilities/kate/-/merge_requests/293)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 4 days.

- **Kate - [Search plugin: fix crash with multiline regexp search](https://invent.kde.org/utilities/kate/-/merge_requests/307)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [Dont use F11 for Toggle-Line-Numbers](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/111)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Improve performance for project loading](https://invent.kde.org/utilities/kate/-/merge_requests/305)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [More quickopen performance improvement](https://invent.kde.org/utilities/kate/-/merge_requests/304)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Refactor projectplugin quick-dialogs](https://invent.kde.org/utilities/kate/-/merge_requests/303)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **kate-editor.org - [Add config and README for a local development environment](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/26)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged at creation day.

