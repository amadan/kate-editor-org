---
title: Loading Remote Files
author: Dominik Haumann

date: 2012-11-05T21:01:09+00:00
url: /2012/11/05/loading-remote-files/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
Loading huge files over the internet may take quite a long time. In KDE 4.10, Kate Part will show a passive message after 1 second that tells you the file is still being loaded, and, if wanted, the user can abort the loading process. It looks like this:

<img class="aligncenter size-full wp-image-2126" title="Loading huge files over the internet" src="/wp-content/uploads/2012/11/long-loading.png" alt="" width="564" height="298" srcset="/wp-content/uploads/2012/11/long-loading.png 564w, /wp-content/uploads/2012/11/long-loading-300x158.png 300w" sizes="(max-width: 564px) 100vw, 564px" /> 

This also fixes <a title="typing characters while loading (e. g. via fish)" href="https://bugs.kde.org/show_bug.cgi?id=128273" target="_blank">bug #128273</a>. Again, cookies go to Christoph ;)