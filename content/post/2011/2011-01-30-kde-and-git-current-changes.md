---
title: KDE and Git, Current Changes
author: Christoph Cullmann

date: 2011-01-30T10:14:46+00:00
url: /2011/01/30/kde-and-git-current-changes/
categories:
  - Developers
  - KDE
tags:
  - planet

---
Thanks to the massive work of sysadmin and others (like Ian Monroe), kdelibs and kdebase are now converted to Git.

In parallel, the move of all kate related code to the kate.git was done and announced. Kate Part / App + KWrite reside now in kate.git on git.kde.org and this is the central place for kate development, like it was already before, but now without the shadow-copies in three other repositories.

As it has shown in the past, this centralisation of parts which belong together helps to get stuff done for our project. The [Get It!][1] page on kate-editor.org is already updated (as now documentation is in the module, too, and some CMake parameters have changed).

I can only say: If you want to add some feature or hassle with some bug, give it a try. It&#8217;s dead easy to get and compile a fresh Kate (even with a bit older kdelibs around). You are welcome ;)

P.S. Thanks again all the people working on the transition to Git and sysadmin for operating the nice projects.kde.org and git.kde.org services!

 [1]: /get-it/