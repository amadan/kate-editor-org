---
title: Contributors, join and help the KDE e.V.
author: Christoph Cullmann

date: 2012-07-04T14:59:09+00:00
url: /2012/07/04/contributors-join-and-help-the-kde-e-v/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Hi,

just yesterday I got asked by one long time contributor, if he can at all join the KDE e.V. or if he needs to do something special for that. He thought it would be more an invitation only club.

It&#8217;s not ;) Anybody who does contribute to KDE can join us.

KDE e.V. definition from its <a href="http://ev.kde.org/" title="KDE e.V." target="_blank">homepage</a>:  
&#8220;KDE e.V. is a registered non-profit organization that represents the KDE Community in legal and financial matters.&#8221;

If you want to have a vote on such stuff and are ok with the rules and goals of the e.V. like you can read on <a href="http://ev.kde.org/" title="KDE e.V." target="_blank">the e.V. homepage</a>, please join.

You just need to fill out the questionaire on <a href="http://ev.kde.org/resources/" title="Forms for joining" target="_blank">the e.V. page</a> and send it to some e.V. member you know and that will support your application.

Even if you are no contributor, but want to support us with money to make our conferences & sprints possible (and our infrastructure), please take a look at our <a href="http://jointhegame.kde.org/" title="Join The Game" target="_blank">&#8220;Join The Game&#8221;</a> campaign.