---
title: Kate “master” branch now KF5 based!
author: Christoph Cullmann

date: 2014-07-24T18:18:55+00:00
url: /2014/07/24/kate-master-branch-now-kf5-based/
categories:
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Hi,

from today on, the master branch of kate.git is KF5 based.

That means, for the next KDE applications release after 4.14, Kate will use the awesome KF5 stuff!

The KTextEditor framework is already in a good shape and most active KatePart development is since months pure KF5 based.

The same should now be true for Kate itself (and KWrite).

Pâté will need a lot of love, as PyQt5 is there, but PyKDE5/PyKF5 still in the works.

Happy hacking, help us to make KF5 based Kate awesome, too.

For 4.14: keep fixing bugs, but all new shiny stuff that might regress things really should only go into our KF5 based master branch!