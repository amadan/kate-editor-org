---
title: Debugging Kate with Qt Creator
author: Dominik Haumann

date: 2010-06-19T09:48:00+00:00
url: /2010/06/19/kate-debugging-with-qt-creator/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2010/06/kate-debugging-with-qt-creator.html
categories:
  - Developers

---
[<img class="size-full wp-image-336" title="qtcreator-sidebar" src="/wp-content/uploads/2010/06/qtcreator-sidebar.png" alt="" width="71" height="542" />][1]

Let&#8217;s have a quick look at how to debug Kate and KWrite with Qt Creator. First, make sure you meet the requirements:

  1. <a title="Building Kate" href="/get-it/" target="_self">build Kate according to this tutorial</a>
  2. <a href="http://www.qt.io/ide/" target="_blank">install Qt-Creator</a> (in my case this is version 2.0.0)

Setup the Kate project Qt-Creator once like this

  1. start Qt-Creator like this (**important to get all environment variables right**):  
    <span style="font-family: courier new;">~/kde/run.sh qtcreator</span>
  2. invoke <span style="font-family: courier new;">File > Open File or Project</span> and choose <span style="font-family: courier new;">~/kde/kate/CMakeLists.txt</span>
  3. Build Location: choose <span style="font-family: courier new;">~/kde/build</span>
  4. Run CMake arguments: type  
    <span style="font-family: courier new;">../kate -DCMAKE_BUILD_TYPE=debugfull -DCMAKE_INSTALL_PREFIX=~/kde/usr</span>
  5. click the button &#8220;Run CMake&#8221; and then &#8220;Finish&#8221;

Start debugging like this

  1. click the **(C)** &#8220;hammer&#8221; icon button on the very bottom left to compile Kate
  2. click the **(B)** &#8220;computer&#8221; icon button and choose &#8220;kate&#8221; (or &#8220;kwrite&#8221;) in the Run combo box
  3. choose the **(A)** &#8220;Debug&#8221; icon in the left pane
  4. invoke &#8220;Debug > Start Debugging (F5)&#8221;, now Kate starts
  5. open part/document/katedocument.cpp in the file tree view on the left
  6. go to the line &#8220;<span style="font-family: courier new;">KateDocument::insertText</span>&#8221; and click &#8220;Debug > Toggle Breakpoint (F9)&#8221;
  7. now if you type a character in Kate, Qt-Crator will halt in <span style="font-family: courier new;">KateDocument::insertText</span>
  8. chose &#8220;Debug > Step Opver (F10)&#8221; (and &#8220;Debug > Stip Into (F11)&#8221;) to step through the code
  9. click on &#8220;Locals and Watchers&#8221; in the debugging pane on the bottom and you see the values of local variables

Happy debugging! :)

 [1]: /wp-content/uploads/2010/06/qtcreator-sidebar.png