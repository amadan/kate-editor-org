---
title: Writing a Kate Plugin
author: Christoph Cullmann

date: 2004-01-06T15:50:59+00:00
url: /2004/01/06/writing-a-kate-plugin/
categories:
  - Developers

---

{{< alert title="Warning" color="warning" >}}
This is an outdated tutorial and is only kept here for historical reasons. For an up-to-date version of the tutorial please check https://develop.kde.org/docs/extend/kate/plugin/
{{< /alert >}}

### Introduction

First at all, why writing plugins for an editor ? Good question, and I hope I have a good answer: Because we want Kate to be small and all extended features not all users need should go into plugins (like CVS suppport, project managment, coffee cooking ;) Therefore Kate provides a quite full-featured plugin interface and interfaces to all important stuff in the Kate application (the documents, views, windows, sidebar &#8230;).  
<!--break-->

This tutorial is for people knowing the Qt/KDE libraries and will not describe how to compile, make &#8230; a kde/qt application/library in detail.

The &#8220;helloworld&#8221; plugin which is here described can be found in the &#8220;kdesdk&#8221; package of kde (located in kdesdk/kate/plugins/helloworld). A detailed description of the Kate API is available [here.][1].

### Coding example: The Konsole plugin

This is one of standard plugins shipped with Kate which embeds the awesome KonsolePart into it to have a nice terminal emulator around.

### Needed files for a plugin

Each plugin consists of at least three files which are needed:

  * a desktop file with some information about the plugin (correct syntax described later &#8211; named like kate&#8221;pluginname&#8221;.desktop
  * a XmlGui rc file named ui.rc
  * a library containing the plugin (named like kate&#8221;pluginname&#8221;plugin)

#### About the desktop file

The desktop file needs the syntax shown here and must be located in kde services directory with the name kate&#8221;yourpluginname&#8221;.desktop,

Example: katekonsoleplugin.desktop

<pre>[Desktop Entry]
Type=Service
ServiceTypes=Kate/Plugin
X-KDE-Library=katekonsoleplugin
X-Kate-Version=2.8
Name=Terminal tool view
Comment=Toolview embedding a terminal widget
X-Kate-MajorProfiles=Kate
X-Kate-MinorProfiles=*
</pre>

**The &#8220;X-Kate-Version=2.8&#8221; line is important! Kate for KDE 4 and up won&#8217;t load your plugin unless the property is similar to the version with which you want to use the plugin!**

To install this desktop file to KDE&#8217;s services directory, we&#8217;ll later add the following line to the CMakeLists.txt file:

<pre>install( FILES katekonsoleplugin.desktop  DESTINATION  ${SERVICES_INSTALL_DIR} )
</pre>

#### About the xmlGUI rc file

The xmlgui resource file describes how the menu and toolbar actions appear in the Kate application. For more detailed examples look into other plugins. The ui.rc file for the katekonsoleplugin looks like this:

<pre>&lt;!DOCTYPE kpartgui&gt;
&lt;gui name="katekonsole" library="katekonsoleplugin" version="3"&gt;
&lt;MenuBar&gt;
 &lt;Menu name="tools"&gt;&lt;text&gt;&amp;Tools&lt;/text&gt;
 &lt;Action name="katekonsole_tools_pipe_to_terminal" group="tools_operations"/&gt;
 &lt;Action name="katekonsole_tools_sync" group="tools_operations"/&gt;
 &lt;Action name="katekonsole_tools_toggle_focus" group="tools_operations"/&gt;
 &lt;/Menu&gt;
&lt;/MenuBar&gt;
&lt;/gui&gt;
</pre>

To automatically install the ui.rc file into the right directory later we add the following line to CMakeLists.txt:

<pre>install( FILES ui.rc  DESTINATION  ${DATA_INSTALL_DIR}/kate/plugins/katekonsole )</pre>

#### About plugin lib

Each Kate plugin is a library which Kate will dynamically load on demand via KLibLoader. The name of the library is kate&#8221;yourpluginname&#8221;plugin.so and should be installed into KDE&#8217;s plugin directory.

The complete result for the file CMakeLists.txt looks like this:

<pre>########### compile and link and install plugin library ###############
set(katekonsoleplugin_PART_SRCS kateconsole.cpp )
kde4_add_plugin(katekonsoleplugin ${katekonsoleplugin_PART_SRCS})
target_link_libraries(katekonsoleplugin  ${KDE4_KDEUI_LIBS} ${KDE4_KPARTS_LIBS} kateinterfaces )
install(TARGETS katekonsoleplugin  DESTINATION ${PLUGIN_INSTALL_DIR} )

########### install support files ###############
install( FILES ui.rc  DESTINATION  ${DATA_INSTALL_DIR}/kate/plugins/katekonsole )
install( FILES katekonsoleplugin.desktop  DESTINATION  ${SERVICES_INSTALL_DIR} )</pre>

<!--pagebreak-->

### Start the real coding ;)

Now we can start with the real coding, The Konsole plugin consists of 2 files, kateconsole.cpp and .h.

### The plugin header file &#8211; kateconsole.h

<pre>#ifndef __KATE_CONSOLE_H__
#define __KATE_CONSOLE_H__

#include &lt;kate/plugin.h&gt;
#include &lt;kate/mainwindow.h&gt;
#include &lt;kate/pluginconfigpageinterface.h&gt;
#include &lt;kurl.h&gt;
#include &lt;kxmlguiclient.h&gt;

#include &lt;kvbox.h&gt;
#include &lt;QList&gt;

class QShowEvent;

namespace KParts
{
 class ReadOnlyPart;
}

namespace KateMDI
{
 }

class KateConsole;
class KateKonsolePluginView;

class KateKonsolePlugin: public Kate::Plugin, public Kate::PluginConfigPageInterface
{
 Q_OBJECT
 Q_INTERFACES(Kate::PluginConfigPageInterface)
 
 friend class KateKonsolePluginView;
 
 public:
 explicit KateKonsolePlugin( QObject* parent = 0, const QList&lt;QVariant&gt;& = QList&lt;QVariant&gt;() );
 virtual ~KateKonsolePlugin();

 Kate::PluginView *createView (Kate::MainWindow *mainWindow);

 // PluginConfigPageInterface
 uint configPages() const { return 1; };
 Kate::PluginConfigPage *configPage (uint number = 0, QWidget *parent = 0, const char *name = 0);
 QString configPageName (uint number = 0) const;
 QString configPageFullName (uint number = 0) const;
 KIcon configPageIcon (uint number = 0) const;

 void readConfig();

 QByteArray previousEditorEnv() {return m_previousEditorEnv;}
 
 private:
 QList&lt;KateKonsolePluginView*&gt; mViews;
 QByteArray m_previousEditorEnv;
};

class KateKonsolePluginView : public Kate::PluginView
{
 Q_OBJECT

 public:
 /**
 * Constructor.
 */
 KateKonsolePluginView (KateKonsolePlugin* plugin, Kate::MainWindow *mainWindow);

 /**
 * Virtual destructor.
 */
 ~KateKonsolePluginView ();

 void readConfig();

 private:
 KateKonsolePlugin *m_plugin;
 KateConsole *m_console;
};

/**
 * KateConsole
 * This class is used for the internal terminal emulator
 * It uses internally the konsole part, thx to konsole devs :)
 */
class KateConsole : public KVBox, public Kate::XMLGUIClient
{
 Q_OBJECT

 public:
 /**
 * construct us
 * @param mw main window
 * @param parent toolview
 */
 KateConsole (KateKonsolePlugin* plugin, Kate::MainWindow *mw, QWidget* parent);

 /**
 * destruct us
 */
 ~KateConsole ();

 void readConfig();

 /**
 * cd to dir
 * @param url given dir
 */
 void cd (const KUrl &url);

 /**
 * send given text to console
 * @param text commands for console
 */
 void sendInput( const QString& text );

 Kate::MainWindow *mainWindow()
 {
 return m_mw;
 }

 public Q_SLOTS:
 /**
 * pipe current document to console
 */
 void slotPipeToConsole ();

 /**
 * synchronize the konsole with the current document (cd to the directory)
 */
 void slotSync();
 /**
 * When syncing is done by the user, also show the terminal if it is hidden
 */
 void slotManualSync();

 private Q_SLOTS:
 /**
 * the konsole exited ;)
 * handle that, hide the dock
 */
 void slotDestroyed ();

 /**
 * construct console if needed
 */
 void loadConsoleIfNeeded();

 /**
 * set or clear focus as appropriate.
 */
 void slotToggleFocus();

 protected:
 /**
 * the konsole get shown
 * @param ev show event
 */
 void showEvent(QShowEvent *ev);

 private:
 /**
 * console part
 */
 KParts::ReadOnlyPart *m_part;

 /**
 * main window of this console
 */
 Kate::MainWindow *m_mw;

 /**
 * toolview for this console
 */
 QWidget *m_toolView;

 KateKonsolePlugin *m_plugin;
};

class KateKonsoleConfigPage : public Kate::PluginConfigPage {
 Q_OBJECT
 public:
 explicit KateKonsoleConfigPage( QWidget* parent = 0, KateKonsolePlugin *plugin = 0 );
 virtual ~KateKonsoleConfigPage()
 {}

 virtual void apply();
 virtual void reset();
 virtual void defaults()
 {}
 private:
 class QCheckBox *cbAutoSyncronize;
 class QCheckBox *cbSetEditor;
 KateKonsolePlugin *mPlugin;
};
#endif

// kate: space-indent on; indent-width 2; replace-tabs on;
</pre>

### The plugin source file &#8211; kateconsole.cpp

<pre>#include "kateconsole.h"
#include "kateconsole.moc"

#include &lt;kicon.h&gt;
#include &lt;kiconloader.h&gt;
#include &lt;ktexteditor/document.h&gt;
#include &lt;ktexteditor/view.h&gt;

#include &lt;kde_terminal_interface.h&gt;
#include &lt;kshell.h&gt;
#include &lt;kparts/part.h&gt;
#include &lt;kaction.h&gt;
#include &lt;kactioncollection.h&gt;
#include &lt;KDialog&gt;

#include &lt;kurl.h&gt;
#include &lt;klibloader.h&gt;
#include &lt;klocale.h&gt;
#include &lt;kdebug.h&gt;
#include &lt;kmessagebox.h&gt;
//Added by qt3to4:
#include &lt;QShowEvent&gt;
#include &lt;QLabel&gt;

#include &lt;QCheckBox&gt;
#include &lt;QVBoxLayout&gt;

#include &lt;kpluginloader.h&gt;
#include &lt;kaboutdata.h&gt;
#include &lt;kpluginfactory.h&gt;
#include &lt;kauthorized.h&gt;

K_PLUGIN_FACTORY(KateKonsoleFactory, registerPlugin&lt;KateKonsolePlugin&gt;();)
K_EXPORT_PLUGIN(KateKonsoleFactory(KAboutData("katekonsole","katekonsoleplugin",ki18n("Konsole"), "0.1", ki18n("Embedded Konsole"), KAboutData::License_LGPL_V2)) )

KateKonsolePlugin::KateKonsolePlugin( QObject* parent, const QList&lt;QVariant&gt;& ):
 Kate::Plugin ( (Kate::Application*)parent )
{
 m_previousEditorEnv=qgetenv("EDITOR");
 if (!KAuthorized::authorizeKAction("shell_access"))
 {
 KMessageBox::sorry(0, i18n ("You do not have enough karma to access a shell or terminal emulation"));
 }
}

KateKonsolePlugin::~KateKonsolePlugin()
{
 ::setenv( "EDITOR", m_previousEditorEnv.data(), 1 );
}

Kate::PluginView *KateKonsolePlugin::createView (Kate::MainWindow *mainWindow)
{
 KateKonsolePluginView *view = new KateKonsolePluginView (this, mainWindow);
 return view;
}

Kate::PluginConfigPage *KateKonsolePlugin::configPage (uint number, QWidget *parent, const char *name)
{
 Q_UNUSED(name)
 if (number != 0)
 return 0;
 return new KateKonsoleConfigPage(parent, this);
}

QString KateKonsolePlugin::configPageName (uint number) const
{
 if (number != 0) return QString();
 return i18n("Terminal");
}

QString KateKonsolePlugin::configPageFullName (uint number) const
{
 if (number != 0) return QString();
 return i18n("Terminal Settings");
}

KIcon KateKonsolePlugin::configPageIcon (uint number) const
{
 if (number != 0) return KIcon();
 return KIcon("utilities-terminal");
}

void KateKonsolePlugin::readConfig()
{
 foreach ( KateKonsolePluginView *view, mViews )
 view-&gt;readConfig();
}

KateKonsolePluginView::KateKonsolePluginView (KateKonsolePlugin* plugin, Kate::MainWindow *mainWindow)
 : Kate::PluginView (mainWindow),m_plugin(plugin)
{
 // init console
 QWidget *toolview = mainWindow-&gt;createToolView ("kate_private_plugin_katekonsoleplugin", Kate::MainWindow::Bottom, SmallIcon("utilities-terminal"), i18n("Terminal"));
 m_console = new KateConsole(m_plugin, mainWindow, toolview);
 
 // register this view
 m_plugin-&gt;mViews.append ( this );
}

KateKonsolePluginView::~KateKonsolePluginView ()
{
 // unregister this view
 m_plugin-&gt;mViews.removeAll (this);
 
 // cleanup, kill toolview + console
 QWidget *toolview = m_console-&gt;parentWidget();
 delete m_console;
 delete toolview;
}

void KateKonsolePluginView::readConfig()
{
 m_console-&gt;readConfig();
}

KateConsole::KateConsole (KateKonsolePlugin* plugin, Kate::MainWindow *mw, QWidget *parent)
 : KVBox (parent), Kate::XMLGUIClient(KateKonsoleFactory::componentData())
 , m_part (0)
 , m_mw (mw)
 , m_toolView (parent)
 , m_plugin(plugin)
{
 QAction* a = actionCollection()-&gt;addAction("katekonsole_tools_pipe_to_terminal");
 a-&gt;setIcon(KIcon("utilities-terminal"));
 a-&gt;setText(i18nc("@action", "&Pipe to Terminal"));
 connect(a, SIGNAL(triggered()), this, SLOT(slotPipeToConsole()));

 a = actionCollection()-&gt;addAction("katekonsole_tools_sync");
 a-&gt;setText(i18nc("@action", "S&ynchronize Terminal with Current Document"));
 connect(a, SIGNAL(triggered()), this, SLOT(slotManualSync()));

 a = actionCollection()-&gt;addAction("katekonsole_tools_toggle_focus");
 a-&gt;setIcon(KIcon("utilities-terminal"));
 a-&gt;setText(i18nc("@action", "&Focus Terminal"));
 connect(a, SIGNAL(triggered()), this, SLOT(slotToggleFocus()));

 m_mw-&gt;guiFactory()-&gt;addClient (this);

 readConfig();
}

KateConsole::~KateConsole ()
{ 
 m_mw-&gt;guiFactory()-&gt;removeClient (this);
 if (m_part)
 disconnect ( m_part, SIGNAL(destroyed()), this, SLOT(slotDestroyed()) );
}

void KateConsole::loadConsoleIfNeeded()
{
 if (m_part) return;

 if (!window() || !parentWidget()) return;
 if (!window() || !isVisibleTo(window())) return;

 KPluginFactory *factory = KPluginLoader("libkonsolepart").factory();

 if (!factory) return;

 m_part = static_cast&lt;KParts::ReadOnlyPart *&gt;(factory-&gt;create&lt;QObject&gt;(this, this));

 if (!m_part) return;

 // start the terminal
 qobject_cast&lt;TerminalInterface*&gt;(m_part)-&gt;showShellInDir( QString() );

 KGlobal::locale()-&gt;insertCatalog("konsole");

 setFocusProxy(m_part-&gt;widget());
 m_part-&gt;widget()-&gt;show();

 connect ( m_part, SIGNAL(destroyed()), this, SLOT(slotDestroyed()) );

 slotSync();
}

void KateConsole::slotDestroyed ()
{
 m_part = 0;

 // hide the dockwidget
 if (parentWidget())
 {
 m_mw-&gt;hideToolView (m_toolView);
 m_mw-&gt;centralWidget()-&gt;setFocus ();
 }
}

void KateConsole::showEvent(QShowEvent *)
{
 if (m_part) return;

 loadConsoleIfNeeded();
}

void KateConsole::cd (const KUrl &url)
{
 sendInput("cd " + KShell::quoteArg(url.path()) + '\n');
}

void KateConsole::sendInput( const QString& text )
{
 loadConsoleIfNeeded();

 if (!m_part) return;

 TerminalInterface *t = qobject_cast&lt;TerminalInterface *&gt;(m_part);

 if (!t) return;

 t-&gt;sendInput (text);
}

void KateConsole::slotPipeToConsole ()
{
 if (KMessageBox::warningContinueCancel
 (m_mw-&gt;window()
 , i18n ("Do you really want to pipe the text to the console? This will execute any contained commands with your user rights.")
 , i18n ("Pipe to Terminal?")
 , KGuiItem(i18n("Pipe to Terminal")), KStandardGuiItem::cancel(), "Pipe To Terminal Warning") != KMessageBox::Continue)
 return;

 KTextEditor::View *v = m_mw-&gt;activeView();

 if (!v)
 return;

 if (v-&gt;selection())
 sendInput (v-&gt;selectionText());
 else
 sendInput (v-&gt;document()-&gt;text());
}

void KateConsole::slotSync()
{
 if (m_mw-&gt;activeView() ) {
 if ( m_mw-&gt;activeView()-&gt;document()-&gt;url().isValid()
 && m_mw-&gt;activeView()-&gt;document()-&gt;url().isLocalFile() ) {
 cd(KUrl( m_mw-&gt;activeView()-&gt;document()-&gt;url().directory() ));
 } else if ( !m_mw-&gt;activeView()-&gt;document()-&gt;url().isEmpty() ) {
 sendInput( "### " + i18n("Sorry, can not cd into '%1'", m_mw-&gt;activeView()-&gt;document()-&gt;url().directory() ) + '\n' );
 }
 }
}

void KateConsole::slotManualSync()
{
 slotSync();
 if ( ! m_part || ! m_part-&gt;widget()-&gt;isVisible() )
 m_mw-&gt;showToolView( parentWidget() );
}
void KateConsole::slotToggleFocus()
{
 QAction *action = actionCollection()-&gt;action("katekonsole_tools_toggle_focus");
 if ( ! m_part ) {
 m_mw-&gt;showToolView( parentWidget() );
 action-&gt;setText( i18n("Defocus Terminal") );
 return; // this shows and focuses the konsole
 }

 if ( ! m_part ) return;

 if (m_part-&gt;widget()-&gt;hasFocus()) {
 if (m_mw-&gt;activeView())
 m_mw-&gt;activeView()-&gt;setFocus();
 action-&gt;setText( i18n("Focus Terminal") );
 } else {
 // show the view if it is hidden
 if (parentWidget()-&gt;isHidden())
 m_mw-&gt;showToolView( parentWidget() );
 else // should focus the widget too!
 m_part-&gt;widget()-&gt;setFocus( Qt::OtherFocusReason );
 action-&gt;setText( i18n("Defocus Terminal") );
 }
}

void KateConsole::readConfig()
{
 disconnect( m_mw, SIGNAL(viewChanged()), this, SLOT(slotSync()) );
 if ( KConfigGroup(KGlobal::config(), "Konsole").readEntry("AutoSyncronize", false) )
 connect( m_mw, SIGNAL(viewChanged()), SLOT(slotSync()) );
 
 
 if ( KConfigGroup(KGlobal::config(), "Konsole").readEntry("SetEditor", false) )
 ::setenv( "EDITOR", "kate -b",1);
 else
 ::setenv( "EDITOR", m_plugin-&gt;previousEditorEnv().data(), 1 );
}

KateKonsoleConfigPage::KateKonsoleConfigPage( QWidget* parent, KateKonsolePlugin *plugin )
 : Kate::PluginConfigPage( parent )
 , mPlugin( plugin )
{
 QVBoxLayout *lo = new QVBoxLayout( this );
 lo-&gt;setSpacing( KDialog::spacingHint() );

 cbAutoSyncronize = new QCheckBox( i18n("&Automatically synchronize the terminal with the current document when possible"), this );
 lo-&gt;addWidget( cbAutoSyncronize );
 cbSetEditor = new QCheckBox( i18n("Set &EDITOR environment variable to 'kate -b'"), this );
 lo-&gt;addWidget( cbSetEditor );
 QLabel *tmp = new QLabel(this);
 tmp-&gt;setText(i18n("Important: The document has to be closed to make the console application continue"));
 lo-&gt;addWidget(tmp);
 reset();
 lo-&gt;addStretch();
 connect( cbAutoSyncronize, SIGNAL(stateChanged(int)), SIGNAL(changed()) );
 connect( cbSetEditor, SIGNAL(stateChanged(int)), SIGNAL(changed()) );
}

void KateKonsoleConfigPage::apply()
{
 KConfigGroup config(KGlobal::config(), "Konsole");
 config.writeEntry("AutoSyncronize", cbAutoSyncronize-&gt;isChecked());
 config.writeEntry("SetEditor", cbSetEditor-&gt;isChecked());
 config.sync();
 mPlugin-&gt;readConfig();
}

void KateKonsoleConfigPage::reset()
{
 KConfigGroup config(KGlobal::config(), "Konsole");
 cbAutoSyncronize-&gt;setChecked(config.readEntry("AutoSyncronize", false));
 cbSetEditor-&gt;setChecked(config.readEntry("SetEditor", false));
}

// kate: space-indent on; indent-width 2; replace-tabs on;</pre>

<!--pagebreak-->

### How does the Plugin look & work after install ?

You first have to go to the Settings -> Configure Kate -> Kate -> Plugins in Kate and activate the Konsole  plugin.

### More information ? Advanced plugin stuff ?

To get more information about the plugin interface and its advanced functions please look at the [Kate API documentation][1] and the [KTextEditor API documentation][2] or contact us on [our mailing list][3]!

 [1]: http://api.kde.org/4.x-api/kdesdk-apidocs/kate/interfaces/kate/html/index.html
 [2]: http://api.kde.org/4.x-api/kdelibs-apidocs/interfaces/ktexteditor/html/index.html
 [3]: https://mail.kde.org/mailman/listinfo/kwrite-devel
