---
title: Who is Hiring?
author: Dominik Haumann

date: 2018-10-14T18:08:59+00:00
url: /2018/10/14/who-is-hiring/
categories:
  - Developers
tags:
  - planet

---
Just as quick info: For some time, there is a [sticky thread on r/cpp][1] about who is hiring C++ developers. This thread gets cleaned quarterly, so all the open jobs listed there are likely still open. The same was just started on reddit for <a href="https://www.reddit.com/r/Qt5/comments/9o1xay/whos_hiring_qt_devs_q4_2018/" target="_blank" rel="noopener">r/Qt5: Who&#8217;s Hiring Qt Devs &#8211; Q4 2018</a>: So if you are looking for either C++ or Qt jobs, this is a good place to have a look at from time to time. When I just looked for r/Qt5, the list was still empty, but this hopefully changes soon, given that the post was added just some hours ago. :-)

 [1]: https://www.reddit.com/r/cpp/comments/9kig88/whos_hiring_c_devs_q4_2018/