---
title: Python plugin developer guide, part 1
author: shaheed

date: 2012-07-01T11:46:19+00:00
url: /2012/07/01/python-plugin-developer-guide-part-1/
pw_single_layout:
  - "1"
categories:
  - Common
tags:
  - python

---
I&#8217;m a Python newbie, so if you are at least that good :-D, you should be able to dive in and write useful Python plugins for your favourite editor too! This is the first in what I hope will be a series of notes and tutorials to help you along the way.

### **Where can I find examples <del>to steal from</del> inspire me?**

Any good developer knows that a good way to launch into a new area is to look at examples, so here is a list of places I&#8217;ve encountered.

  *  Plugins that came with Kate. These will be in the directory shown when you [open the Python plugin configuration page][1], probably someplace like /usr/share/apps/kate/pate.
  * <http://pypi.python.org/pypi/Kate-plugins>
  * <https://github.com/goinnn/Kate-plugins>
  * <https://github.com/mtorromeo>
  * <https://github.com/zaufi/kate-pate-plugins>

Remember to respect others&#8217; copyrights, and give credit where it is due.

### **Where do I put my code?**

The Pâté Python plugin looks in several places to find usable plugins, you should be able to use the Python console to find a directory you can use for your developments. Select it, and press &#8220;Reload&#8221;:

[<img class="alignnone size-medium wp-image-1864" src="/wp-content/uploads/2012/06/snapshot17-300x178.png" alt="" width="300" height="178" srcset="/wp-content/uploads/2012/06/snapshot17-300x178.png 300w, /wp-content/uploads/2012/06/snapshot17.png 694w" sizes="(max-width: 300px) 100vw, 300px" />][2]

Launch it from the View menu, and then ask KDE where your files should go:

[<img class="alignnone size-medium wp-image-1865" src="/wp-content/uploads/2012/06/snapshot18-300x198.png" alt="" width="300" height="198" srcset="/wp-content/uploads/2012/06/snapshot18-300x198.png 300w, /wp-content/uploads/2012/06/snapshot18.png 677w" sizes="(max-width: 300px) 100vw, 300px" />][3]

Or, if that&#8217;s a bit hard to read, like this:

<p style="padding-left: 30px">
  >>> from PyKDE4.kdecore import *
</p>

<p style="padding-left: 30px">
  >>> print KStandardDirs.locateLocal(&#8220;appdata&#8221;,&#8221;pate&#8221;)
</p>

<p style="padding-left: 30px">
  /home/srhaque/.kde/share/apps/kate/pate
</p>

#### Try the directory

If the given directory does not exist, create it. Press Reload, and the new directory should be visible:

[<img class="alignnone size-medium wp-image-1866" src="/wp-content/uploads/2012/06/snapshot19-300x180.png" alt="" width="300" height="180" srcset="/wp-content/uploads/2012/06/snapshot19-300x180.png 300w, /wp-content/uploads/2012/06/snapshot19.png 680w" sizes="(max-width: 300px) 100vw, 300px" />][4]

You are ready to create your plugin in the new directory! This plugin directory will be added to the initial value of sys.path used for _all_ plugins.

### Plugin structure and naming

Your plugin can be just a single .py file, such as &#8220;console.py&#8221;, or multiple files contained in a directory. A single .py file is useful for simple things, but if you want to use .ui files or multiple Python files, a directory is recommended. If you use this option, the main plugin file must be named after the directory, so:

  * If the directory is called &#8220;console&#8221;
  * The main plugin file must be named &#8220;console/console.py&#8221;

In the directory case, the directory is added to sys.path to load the main plugin file and other Python modules you may have. Since sys.path is _searched_ by Python in order to load modules, having the same plugin in multiple locations would be very confusing. Pâté supports this by refusing to load identically named plugins; instead they are highlighted as being hidden:

[<img class="alignnone size-medium wp-image-1872" src="/wp-content/uploads/2012/07/snapshot20-300x193.png" alt="" width="300" height="193" srcset="/wp-content/uploads/2012/07/snapshot20-300x193.png 300w, /wp-content/uploads/2012/07/snapshot20.png 680w" sizes="(max-width: 300px) 100vw, 300px" />][5]

This also means you can just copy one of the plugins supplied with Kate to get started!

&nbsp;

 [1]: /2012/06/26/python-plugin-user-guide/ "Python plugin configuration page"
 [2]: /wp-content/uploads/2012/06/snapshot17.png
 [3]: /wp-content/uploads/2012/06/snapshot18.png
 [4]: /wp-content/uploads/2012/06/snapshot19.png
 [5]: /wp-content/uploads/2012/07/snapshot20.png