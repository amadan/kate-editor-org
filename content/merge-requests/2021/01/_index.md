---
title: Merge Requests - January 2021
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2021/01/
---

#### Week 04

- **Kate - [Do not restore Projects tool view when no project exists](https://invent.kde.org/utilities/kate/-/merge_requests/218)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [Also set selected foreground in semantic highlighting](https://invent.kde.org/utilities/kate/-/merge_requests/213)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Show dir selection dialog for ProjectConfig-&gt;index](https://invent.kde.org/utilities/kate/-/merge_requests/217)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [quickopen: construct context menu on demand](https://invent.kde.org/utilities/kate/-/merge_requests/215)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Don&#x27;t expand semantic highlighting ranges to prevent incorrect highlighting while typing](https://invent.kde.org/utilities/kate/-/merge_requests/214)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Port away from Qt&#x27;s forever](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/98)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [highlighter_benchmark: ignore .clang-format](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/161)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Use atomic variables for lock free access](https://invent.kde.org/utilities/kate/-/merge_requests/212)**<br />
Request authored by [Anthony Fieroni](https://invent.kde.org/anthonyfieroni) and merged at creation day.

- **KTextEditor - [Increase maximum indentation width to 200](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/97)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [[Vimode] Do not switch view when changing case (~ command)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/96)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **Kate - [improve threading](https://invent.kde.org/utilities/kate/-/merge_requests/211)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Work/fixes](https://invent.kde.org/utilities/kate/-/merge_requests/210)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [Work/clang format](https://invent.kde.org/utilities/kate/-/merge_requests/207)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after one day.

- **KTextEditor - [Add test case for range accessed after deletion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/95)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Less vibrant operator color for Breeze themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/159)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Search disk files multithreaded](https://invent.kde.org/utilities/kate/-/merge_requests/208)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Clean up command bar code](https://invent.kde.org/utilities/kate/-/merge_requests/209)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Konsole plugin: Sync terminal path when the current document&#x27;s url changes (e.g. first document open, save as)](https://invent.kde.org/utilities/kate/-/merge_requests/205)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KTextEditor - [Only show &quot;There are no more chars for next bookmark&quot; error when in vimode](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/93)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **Kate - [LSP: Fix Ctrl + click underline highlighting broken in #include context](https://invent.kde.org/utilities/kate/-/merge_requests/203)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [LSP: Fix highlighting not cleared after commenting code out](https://invent.kde.org/utilities/kate/-/merge_requests/202)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Update Haskell for settings.json](https://invent.kde.org/utilities/kate/-/merge_requests/201)**<br />
Request authored by [hololeap hololeap](https://invent.kde.org/hololeap) and merged at creation day.

- **Kate - [Add Workaround for KateTabBar sometimes disappearing on session load](https://invent.kde.org/utilities/kate/-/merge_requests/199)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KTextEditor - [Use new signal/slot syntax](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/89)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **KTextEditor - [Retain replacement text as long as the power search bar is not closed](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/90)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [[vimode] Fix motion to matching item off-by-one](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/91)**<br />
Request authored by [Arusekk ­­](https://invent.kde.org/arusekk) and merged at creation day.

#### Week 03

- **Kate - [Fix color loading for LSP client plugin](https://invent.kde.org/utilities/kate/-/merge_requests/198)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [KateBookMarks: modernise code base](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/88)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [Fix alpha channel being ignored when reading from config interface](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/87)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use the new theme function to provide better semantic highlighting](https://invent.kde.org/utilities/kate/-/merge_requests/195)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Fixed colors for parameters for some themes](https://invent.kde.org/utilities/kate/-/merge_requests/197)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Work/ahmad/signal syntax 2](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/82)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 3 days.

- **Kate - [Add missing HTML tag](https://invent.kde.org/utilities/kate/-/merge_requests/196)**<br />
Request authored by [Yunhe Guo](https://invent.kde.org/guoyunhe) and merged at creation day.

- **KTextEditor - [Improve the opening bracket preview](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/83)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KTextEditor - [CommandRangeExpressionParser: port QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/85)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [avoid duplicated highlighting ranges that kill ARGB rendering](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/86)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Atom Light, Breeze Dark/Light: new color for Operator ; Breeze Light: new color for ControlFlow](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/154)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KSyntaxHighlighting - [Improve readability of Solarized dark theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/157)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [remove unnecessary captures with a dynamic rule](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/156)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [add const overloads for some accessors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/158)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **kate-editor.org - [Fix translations](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/14)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Use editor theme colors to theme Search&amp;Replace plugin](https://invent.kde.org/utilities/kate/-/merge_requests/194)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix extra inverted comma on autocomplete with autobrackets](https://invent.kde.org/utilities/kate/-/merge_requests/192)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [expose the global KSyntaxHighlighting::Repository read-only](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/84)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Correct indentation bug when line contains &quot;for&quot; or &quot;else&quot;.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/79)**<br />
Request authored by [Francis Laniel](https://invent.kde.org/eiffel) and merged after 2 days.

- **kate-editor.org - [Fix button translation in Get It page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/13)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **kate-editor.org - [Fix minor typo: This -&gt; These](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/12)**<br />
Request authored by [Yuri Chornoivan](https://invent.kde.org/yurchor) and merged at creation day.

- **Kate - [try to ignore tabbar shortcuts](https://invent.kde.org/utilities/kate/-/merge_requests/189)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Draw tiny buttons for shortcuts instead of just text](https://invent.kde.org/utilities/kate/-/merge_requests/188)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Add a couple of extra checks for sanity and remove commented code](https://invent.kde.org/utilities/kate/-/merge_requests/191)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix compilation warning](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/81)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Bash, Zsh: fix cmd;; in a case](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/155)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **kate-editor.org - [Improve Get It page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/11)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Add documentation for copy, cut and paste view functions.](https://invent.kde.org/utilities/kate/-/merge_requests/187)**<br />
Request authored by [Francis Laniel](https://invent.kde.org/eiffel) and merged at creation day.

- **Kate - [Draw rectancle inline color notes including a hover effect](https://invent.kde.org/utilities/kate/-/merge_requests/181)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 2 days.

- **KTextEditor - [CMake: Use configure_file() to ensure noop incremental builds](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/80)**<br />
Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer) and merged at creation day.

- **Kate - [Add a quick-open style command bar](https://invent.kde.org/utilities/kate/-/merge_requests/179)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Use erase/remove_if instead of loop to avoid massive random vector element removal costs](https://invent.kde.org/utilities/kate/-/merge_requests/185)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Optimize search some more and reduce memory usage](https://invent.kde.org/utilities/kate/-/merge_requests/184)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Tweak fuzzy matcher to score sequential matches a bit higher for kate command bar](https://invent.kde.org/utilities/kate/-/merge_requests/183)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Allow &#x27;QMenu&#x27; inside the command bar](https://invent.kde.org/utilities/kate/-/merge_requests/182)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Correct an indentation bug.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/70)**<br />
Request authored by [Francis Laniel](https://invent.kde.org/eiffel) and merged after 3 days.

#### Week 02

- **Kate - [Use an abstract item model for search and replace matches](https://invent.kde.org/utilities/kate/-/merge_requests/180)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Unbreak quickopen&#x27;s reslect first](https://invent.kde.org/utilities/kate/-/merge_requests/178)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Avoid module-wide includes (QtTest and QtDBus)](https://invent.kde.org/utilities/kate/-/merge_requests/177)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [Work/clang format stuff](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/78)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **KTextEditor - [paint the small gap in selection color, if previous line end is in selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/77)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Fix indent for when pressing enter and the function param has a comma at the end](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/76)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [avoid full line selection painting, more in line with other editors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/75)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Port remaining uses of QRegExp in src/vimode into QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/67)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 4 days.

- **KTextEditor - [Include QTest instead of QtTest (to avoid module-wide includes)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/74)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KSyntaxHighlighting - [email.xml: Detect nested comments and escaped characters](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/153)**<br />
Request authored by [Martin Walch](https://invent.kde.org/martinwalch) and merged at creation day.

- **KSyntaxHighlighting - [Update atom light to use alpha colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/152)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Respect alpha colors while exporting html](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/73)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Alpha colors support: Config layer](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/72)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Current line highlighting over full icon border](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/71)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [remap some Symbol and Operator styles to dsOperator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/150)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Bash: fix } in ${!xy*} and more Parameter Expansion Operator (# in ${#xy} ;...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/151)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Dont override the cursor for whole application when building](https://invent.kde.org/utilities/kate/-/merge_requests/175)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Enable alpha channel for editor colors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/69)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Allow using the alpha channel in theme colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/148)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KSyntaxHighlighting - [Update monokai colors and fix incorrect blue](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/149)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Update Kconfig highlighter to Linux 5.9](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/144)**<br />
Request authored by [Martin Walch](https://invent.kde.org/martinwalch) and merged after 6 days.

- **Kate - [Allow / separated pattern to filter same filename across different directories](https://invent.kde.org/utilities/kate/-/merge_requests/176)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix current line highlight has a tiny gap at the beginning](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/68)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix crash due to invalid pointer when jumping using CtrlClick](https://invent.kde.org/utilities/kate/-/merge_requests/174)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix compilation on Kubuntu 20.04](https://invent.kde.org/utilities/kate/-/merge_requests/173)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [[Vimode] Do not skip folded range when the motion lands inside](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/64)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 4 days.

- **KTextEditor - [Port the (trivial) uses of QRegExp in normal vimode to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/62)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 6 days.

- **KSyntaxHighlighting - [C++: fix us suffix](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/147)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Bash: fix #5: $ at the end of a double quoted string](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/146)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [LSP Client: Jump to Declaration / Definition on Control-click](https://invent.kde.org/utilities/kate/-/merge_requests/172)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [VHDL: fix function, procedure, type range/units and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/145)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

#### Week 01

- **Kate - [Improve the ctags-GotoSymbol plugin](https://invent.kde.org/utilities/kate/-/merge_requests/171)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use fuzzy filter in kate lspclientplugin symbol view](https://invent.kde.org/utilities/kate/-/merge_requests/170)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [expose the KSyntaxHighlighting theme](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/66)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [add configChanged to KTextEditor::Editor, too, for global configuration changes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/65)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Update breeze-dark.theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/143)**<br />
Request authored by [Andrey Karepin](https://invent.kde.org/akarepin) and merged after one day.

- **Kate - [Improve the UI for quick-open](https://invent.kde.org/utilities/kate/-/merge_requests/168)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Dont show absouloute path for files in quick-open](https://invent.kde.org/utilities/kate/-/merge_requests/167)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use fuzzy filter in kateprojectplugin](https://invent.kde.org/utilities/kate/-/merge_requests/166)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix html tags appearing in ctags-goto-symbol](https://invent.kde.org/utilities/kate/-/merge_requests/163)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Improve text for goto symbol actions](https://invent.kde.org/utilities/kate/-/merge_requests/165)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [allow to access &amp; alter the current theme via config interface](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/63)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Support fuzzy matching for quick open/...](https://invent.kde.org/utilities/kate/-/merge_requests/140)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 20 days.

- **KTextEditor - [Minor QRegularExpression optimisations](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/60)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Raku: #7: fix symbols that starts with Q](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/141)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 53

- **KTextEditor - [Add KTextEditor::LineRange](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/57)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after one day.

- **KTextEditor - [Cursor, Range: Add fromString(QStringView) overload](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/58)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after one day.

- **KTextEditor - [Allow &quot;Dynamic Word Wrap Align Indent&quot; to be disabled](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/59)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KSyntaxHighlighting - [Add Oblivion color scheme from GtkSourceView/Pluma/gEdit](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/132)**<br />
Request authored by [Yuki Schlarb](https://invent.kde.org/ntninja) and merged after 7 days.

- **Kate - [batch matches together to avoid massive costs for repeated model update](https://invent.kde.org/utilities/kate/-/merge_requests/159)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [UrlInfo: handle filenames starting with &#x27;:&#x27;](https://invent.kde.org/utilities/kate/-/merge_requests/134)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 21 days.

- **Kate - [Cache colors outside the loop](https://invent.kde.org/utilities/kate/-/merge_requests/162)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [[Vimode]Fix search inside fold ranges](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/53)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 6 days.

- **KTextEditor - [[Vimode] Fix Macro Completion Replay](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/52)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 8 days.

- **KTextEditor - [Improve search performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/56)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [avoid stall at end of search if not expand all set + show intermediate match count](https://invent.kde.org/utilities/kate/-/merge_requests/161)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [allow to ignore binary files for project based search, too](https://invent.kde.org/utilities/kate/-/merge_requests/158)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Dont reconstruct the same i18n string everytime without any reason](https://invent.kde.org/utilities/kate/-/merge_requests/157)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

