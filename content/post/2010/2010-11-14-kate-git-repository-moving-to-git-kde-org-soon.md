---
title: Kate git repository moving to git.kde.org soon!
author: Christoph Cullmann

date: 2010-11-14T18:40:32+00:00
url: /2010/11/14/kate-git-repository-moving-to-git-kde-org-soon/
categories:
  - Common
tags:
  - planet

---
We tried to move weeks ago already, but in our current repository some small glitches occured during the SVN => Git transition.  
I have redone the conversion now and soon we will be on git.kde.org (if I not screw that again, thanks already to sysadmin for all help, now and in past!).  
Still some small stuff will then need to be ported, which I will do after the initial repository is up there.  
After this, the repository on Gitorious will vanish.

I will keep syncing KDE SVN and the repository, until KDE has migrated, too.  
But please, if you work on features rather than simple oneliner fixes, use the git repository on git.kde.org after it is announced to be available.  
You have automagically a git account and it makes merges for me much easier, if there are not too many conflicts from SVN -><- Git. I hope the Kate team can keep the kate repository then as sole workspace, as working on the kate part/app is much easier for all contributors if you don't have to get whole kdelibs/kdesdk. But that is still not discussed out I think, release team has a say here, as I would rather like to stay in KDE release cycle.